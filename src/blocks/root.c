#include <wlblocks/blocks.h>

static void arrange(struct wlb_block *block) {
  struct wlb_root *root = utl_container_of(block, root, block);

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    wlb_block_apply_rect(c, block->rect);
  }
}

static const struct wlb_block_impl impl = {
    .arrange = arrange,
};

struct wlb_root *wlb_root_create(void) {
  struct wlb_root *root = calloc(1, sizeof(struct wlb_root));
  if (!root) return NULL;

  wlb_block_init(&root->block, &impl);

  return root;
}
