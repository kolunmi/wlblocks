#include <linux/input-event-codes.h>
#include <rlgl.h>
#include <wayland-client-protocol.h>
#include <wlblocks/blocks.h>
#include <wlblocks/crdraw.h>
#include <wlblocks/wlb_data.h>

#include "header_bar-ui.h"

static void tree(struct wlb_block *block) {
  struct wlb_header_bar *header_bar = utl_container_of(block, header_bar, block);
  struct wlb_document *doc = header_bar->block.doc;
  struct header_bar_ui *ui = header_bar->private;

  wlb_label_set_text(ui->title, doc->title);

  int height = (doc->theme->font.height + doc->theme->font.height / 2.0) * doc->scale;
  header_bar->block.props.min_height = height;
  header_bar->block.props.max_height = height;
}

static void get_children_parent(struct wlb_block *block,
                                struct wlb_block **parent) {
  struct wlb_header_bar *header_bar = utl_container_of(block, header_bar, block);
  struct header_bar_ui *ui = header_bar->private;

  if (ui) *parent = &ui->children->block;
}

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_header_bar *header_bar = utl_container_of(block, header_bar, block);
  struct wlb_document *doc = header_bar->block.doc;
  struct header_bar_ui *ui = header_bar->private;

  wlb_button_set_icon(ui->maximize, doc->maximized ? WLB_ICON_WINDOWS : WLB_ICON_MAXIMIZE);

  wlb_gl_draw_rect(header_bar->block.rect,
                   doc->focused ? &doc->theme->color_config.focused.border : &doc->theme->color_config.focused.base);
}

static void pointer_button(struct wlb_block *block, int button, int state, int x, int y) {
  if (state != WL_POINTER_BUTTON_STATE_PRESSED || button != BTN_LEFT) return;

  block->dragging = false;
  wlb_signal_emit(&block->doc->events.request_drag, NULL);
}

static void get_pointer_priority(struct wlb_block *block, int *priority) {
  *priority = 1;
}

static void destroy(struct wlb_block *block) {
  struct wlb_header_bar *header_bar = utl_container_of(block, header_bar, block);

  free(header_bar->private);
}

static const struct wlb_block_impl impl = {
    .tree = tree,
    .get_children_parent = get_children_parent,
    .draw = draw,
    .get_pointer_priority = get_pointer_priority,
    .pointer_button = pointer_button,
    .destroy = destroy,
};

static void handle_minimize(struct wlb_listener *listener, void *data) {
  struct wlb_header_bar *header_bar = utl_container_of(listener, header_bar, minimize);

  wlb_signal_emit(&header_bar->block.doc->events.request_minimize, NULL);
}

static void handle_maximize(struct wlb_listener *listener, void *data) {
  struct wlb_header_bar *header_bar = utl_container_of(listener, header_bar, maximize);

  wlb_signal_emit(&header_bar->block.doc->events.request_maximize, NULL);
}

static void handle_close(struct wlb_listener *listener, void *data) {
  struct wlb_header_bar *header_bar = utl_container_of(listener, header_bar, close);

  wlb_signal_emit(&header_bar->block.doc->events.request_close, NULL);
}

struct wlb_header_bar *wlb_header_bar_create(void) {
  struct wlb_header_bar *header_bar = calloc(1, sizeof(struct wlb_header_bar));
  if (!header_bar) return NULL;

  wlb_block_init(&header_bar->block, &impl);

  wlb_signal_init(&header_bar->events.dragged);

  header_bar->props.show_buttons = true;
  header_bar->props.show_title = true;

  struct header_bar_ui *ui = calloc(1, sizeof(struct header_bar_ui));
  if (!ui) goto err;
  if (!make_header_bar_ui(&header_bar->block, ui)) goto err;

  header_bar->private = ui;

  header_bar->minimize.notify = handle_minimize;
  wlb_signal_add(&ui->minimize->events.pressed, &header_bar->minimize);
  header_bar->maximize.notify = handle_maximize;
  wlb_signal_add(&ui->maximize->events.pressed, &header_bar->maximize);
  header_bar->close.notify = handle_close;
  wlb_signal_add(&ui->close->events.pressed, &header_bar->close);

  return header_bar;

err:
  free(ui);
  free(header_bar);
  return NULL;
}
