#include <wlblocks/blocks.h>

#include "scroll_window-ui.h"

static void get_children_parent(struct wlb_block *block,
                                struct wlb_block **parent) {
  struct wlb_scroll_window *scroll_window = utl_container_of(block, scroll_window, block);
  struct scroll_window_ui *ui = scroll_window->private;

  if (ui) *parent = &ui->children->block;
}

static void get_pointer_priority(struct wlb_block *block, int *priority) {
  *priority = 1;
}

static void pointer_scroll(struct wlb_block *block, int vscroll,
                           int hscroll, int x, int y) {
  struct wlb_scroll_window *scroll_window = utl_container_of(block, scroll_window, block);
  struct scroll_window_ui *ui = scroll_window->private;

  if (vscroll != 0) {
    float offset = vscroll * ui->vscroll->props.ratio * 0.01;
    wlb_scrollbar_set_position(ui->vscroll, ui->vscroll->props.position + offset);
  }

  if (hscroll != 0) {
    float offset = hscroll * ui->hscroll->props.ratio * 0.01;
    wlb_scrollbar_set_position(ui->hscroll, ui->hscroll->props.position + offset);
  }
}

static void destroy(struct wlb_block *block) {
  struct wlb_scroll_window *scroll_window = utl_container_of(block, scroll_window, block);

  free(scroll_window->private);
}

static const struct wlb_block_impl impl = {
    .get_children_parent = get_children_parent,
    .get_pointer_priority = get_pointer_priority,
    .pointer_scroll = pointer_scroll,
    .destroy = destroy,
};

static void handle_vscroll(struct wlb_listener *listener, void *data) {
  struct wlb_scroll_window *scroll_window = utl_container_of(listener, scroll_window, vscroll);
  struct scroll_window_ui *ui = scroll_window->private;

  scroll_window->props.position.y = ui->vscroll->props.position *
                                    (1 - ui->vscroll->props.ratio) *
                                    ui->children->block.rect.h;

  wlb_content_set_offset(ui->window, scroll_window->props.position);
}

static void handle_hscroll(struct wlb_listener *listener, void *data) {
  struct wlb_scroll_window *scroll_window = utl_container_of(listener, scroll_window, hscroll);
  struct scroll_window_ui *ui = scroll_window->private;

  scroll_window->props.position.x = ui->hscroll->props.position *
                                    (1 - ui->hscroll->props.ratio) *
                                    ui->children->block.rect.w;

  wlb_content_set_offset(ui->window, scroll_window->props.position);
}

static void recalc_ratio(struct wlb_scroll_window *scroll_window) {
  struct scroll_window_ui *ui = scroll_window->private;

  float ratio = ui->window->block.rect.h / ui->children->block.rect.h;
  wlb_block_set_visible(&ui->vscroll->block, ratio < 1);
  wlb_scrollbar_set_ratio(ui->vscroll, ratio);

  ratio = ui->window->block.rect.w / ui->children->block.rect.w;
  wlb_block_set_visible(&ui->hscroll->block, ratio < 1);
  wlb_scrollbar_set_ratio(ui->hscroll, ratio);
}

static void recalc_scrollbars_soft(struct wlb_scroll_window *scroll_window) {
  struct scroll_window_ui *ui = scroll_window->private;

  ui->hscroll->props.position = scroll_window->props.position.x / (ui->children->block.rect.w - ui->window->block.rect.w);
  ui->vscroll->props.position = scroll_window->props.position.y / (ui->children->block.rect.h - ui->window->block.rect.h);
}

static void handle_content_change(struct wlb_listener *listener, void *data) {
  struct wlb_scroll_window *scroll_window = utl_container_of(listener, scroll_window, content_change);
  struct scroll_window_ui *ui = scroll_window->private;

  recalc_ratio(scroll_window);
  recalc_scrollbars_soft(scroll_window);
}

static void handle_window_change(struct wlb_listener *listener, void *data) {
  struct wlb_scroll_window *scroll_window = utl_container_of(listener, scroll_window, window_change);

  recalc_ratio(scroll_window);
}

struct wlb_scroll_window *wlb_scroll_window_create(void) {
  struct wlb_scroll_window *scroll_window = calloc(1, sizeof(struct wlb_scroll_window));
  if (!scroll_window) return NULL;

  wlb_block_init(&scroll_window->block, &impl);

  wlb_signal_init(&scroll_window->events.scrolled);

  struct scroll_window_ui *ui = calloc(1, sizeof(struct scroll_window_ui));
  if (!ui) goto err;
  if (!make_scroll_window_ui(&scroll_window->block, ui)) goto err;

  scroll_window->private = ui;

  scroll_window->vscroll.notify = handle_vscroll;
  wlb_signal_add(&ui->vscroll->events.scrolled, &scroll_window->vscroll);
  scroll_window->hscroll.notify = handle_hscroll;
  wlb_signal_add(&ui->hscroll->events.scrolled, &scroll_window->hscroll);
  scroll_window->content_change.notify = handle_content_change;
  wlb_signal_add(&ui->children->block.events.resized, &scroll_window->content_change);
  scroll_window->window_change.notify = handle_window_change;
  wlb_signal_add(&ui->window->block.events.resized, &scroll_window->window_change);

  return scroll_window;

err:
  free(ui);
  free(scroll_window);
  return NULL;
}

bool wlb_scroll_window_set_position(struct wlb_scroll_window *scroll_window,
                                    struct wlb_vec2 position) {
  if (!scroll_window) return false;
  struct scroll_window_ui *ui = scroll_window->private;

  scroll_window->props.position = position;

  wlb_content_set_offset(ui->window, position);
  recalc_scrollbars_soft(scroll_window);

  return true;
}
