#include <wlblocks/blocks.h>

#include "canvas-ui.h"
#include "scroll_window-ui.h"

static void get_children_parent(struct wlb_block *block,
                                struct wlb_block **parent) {
  struct wlb_canvas *canvas = utl_container_of(block, canvas, block);

  struct canvas_ui *ui = canvas->private;
  if (!ui) return;

  struct scroll_window_ui *scroll_ui = ui->scroll_window->private;
  if (!scroll_ui) return;

  *parent = &scroll_ui->children->block;
}

static void get_pointer_priority(struct wlb_block *block, int *priority) {
  *priority = block->dragging ? -1 : 0;
}

static void pointer_scroll(struct wlb_block *block, int vscroll,
                           int hscroll, int x, int y) {
  struct wlb_canvas *canvas = utl_container_of(block, canvas, block);
  struct canvas_ui *ui = canvas->private;
  struct scroll_window_ui *scroll_ui = ui->scroll_window->private;

  if (vscroll == 0) return;

  float zoom = vscroll * 0.025;
  float old_scale = scroll_ui->window->props.scale;
  float new_scale = old_scale - zoom;
  if (new_scale < 0.1) new_scale = 0.1;
  float ratio = new_scale / old_scale;

  wlb_content_set_scale(scroll_ui->window, new_scale);

  struct wlb_vec2 mouse = {
      x - scroll_ui->children->block.rect.x,
      y - scroll_ui->children->block.rect.y,
  };
  struct wlb_vec2 offset = {
      mouse.x - ui->scroll_window->props.position.x,
      mouse.y - ui->scroll_window->props.position.y,
  };
  struct wlb_vec2 scaled = {
      mouse.x * ratio,
      mouse.y * ratio,
  };
  struct wlb_vec2 position = {
      scaled.x - offset.x,
      scaled.y - offset.y,
  };

  wlb_scroll_window_set_position(ui->scroll_window, position);
}

static void pointer_move(struct wlb_block *block, int x, int y, int dx, int dy) {
  struct wlb_canvas *canvas = utl_container_of(block, canvas, block);
  struct canvas_ui *ui = canvas->private;

  /* if (!block->hovered) { */
  /*   float nx, ny; */
  /*   if (wlb_rectf_closest_point(&block->rect, x, y, &nx, &ny) && */
  /*       wlb_rectf_closest_point(&block->rect, nx - dx * block->rect.w, ny - dy * block->rect.h, &nx, &ny)) { */
  /*     struct wlb_vec2 warp = {nx, ny}; */
  /*     wlb_signal_emit(&block->doc->events.request_warp, &warp); */
  /*   } */
  /* } */

  if (block->dragging) {
    struct wlb_vec2 new_position = {
        ui->scroll_window->props.position.x - dx,
        ui->scroll_window->props.position.y - dy,
    };
    wlb_scroll_window_set_position(ui->scroll_window, new_position);
  }
}

static void destroy(struct wlb_block *block) {
  struct wlb_canvas *canvas = utl_container_of(block, canvas, block);

  free(canvas->private);
}

static const struct wlb_block_impl impl = {
    .get_children_parent = get_children_parent,
    .get_pointer_priority = get_pointer_priority,
    .pointer_move = pointer_move,
    .pointer_scroll = pointer_scroll,
    .destroy = destroy,
};

struct wlb_canvas *wlb_canvas_create(void) {
  struct wlb_canvas *canvas = calloc(1, sizeof(struct wlb_canvas));
  if (!canvas) return NULL;

  wlb_block_init(&canvas->block, &impl);

  wlb_signal_init(&canvas->events.scrolled);

  struct canvas_ui *ui = calloc(1, sizeof(struct canvas_ui));
  if (!ui) goto err;
  if (!make_canvas_ui(&canvas->block, ui)) goto err;
  canvas->private = ui;

  return canvas;

err:
  free(ui);
  free(canvas);
  return NULL;
}
