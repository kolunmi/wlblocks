#include <math.h>
#include <wlblocks/blocks.h>

static void reclaim(struct wlb_container *container, float to_reclaim) {
  struct wlb_block *block = &container->block;

  int divisor = 0;

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible || c->props.expand) continue;
    float min = container->props.vertical ? c->props.min_height : c->props.min_width;
    float size = container->props.vertical ? c->size_cache.y : c->size_cache.x;
    if (size > min) divisor++;
  }

  bool redo;
  float to_subtract;
  do {
    redo = false;
    to_subtract = to_reclaim / divisor;
    utl_list_for_each(c, &block->children, link) {
      if (!c->props.visible || c->props.expand) continue;

      float min = container->props.vertical ? c->props.min_height : c->props.min_width;
      float size = container->props.vertical ? c->size_cache.y : c->size_cache.x;
      if (size <= min) continue;

      if (size - to_subtract < min) {
        to_reclaim -= size - min;
        divisor--;
        if (container->props.vertical) {
          c->size_cache.y = min;
        } else {
          c->size_cache.x = min;
        }
        redo = true;
        break;
      }
    }
  } while (redo);

  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible || c->props.expand) continue;

    float min = container->props.vertical ? c->props.min_height : c->props.min_width;
    if (container->props.vertical) {
      if (c->size_cache.y > min) c->size_cache.y -= to_subtract;
    } else {
      if (c->size_cache.x > min) c->size_cache.x -= to_subtract;
    }
  }
}

/* FIXME this entire function needs to be cleaned up */
static void request_size(struct wlb_block *block,
                         struct wlb_vec2 hint,
                         struct wlb_vec2 *size) {
  struct wlb_container *container = utl_container_of(block, container, block);

  if (container->props.vertical) {
    size->x = 0;
    size->y = container->props.margin;
    if (hint.x >= 0) {
      hint.x -= 2 * container->props.margin;
      if (hint.x < 0) hint.x = 0;
    }
  } else {
    size->x = container->props.margin;
    size->y = 0;
    if (hint.y >= 0) {
      hint.y -= 2 * container->props.margin;
      if (hint.y < 0) hint.y = 0;
    }
  }

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible || c->props.expand) continue;

    struct wlb_vec2 req_size;
    wlb_block_get_request_size(c, hint, &req_size);

    if (container->props.vertical) {
      size->x = fmaxf(size->x, req_size.x);
      size->y += req_size.y + container->props.margin;
    } else {
      size->x += req_size.x + container->props.margin;
      size->y = fmaxf(size->y, req_size.y);
    }
  }

  if (hint.x >= 0 || hint.y >= 0) {
    float hint_dim = container->props.vertical ? hint.y : hint.x;
    if (hint_dim >= 0) {
      float size_dim = container->props.vertical ? size->y : size->x;
      if (size_dim > hint_dim) reclaim(container, size_dim - hint_dim);

      if (container->props.vertical) {
        size->x = 0;
        size->y = container->props.margin;
      } else {
        size->x = container->props.margin;
        size->y = 0;
      }

      utl_list_for_each(c, &block->children, link) {
        if (!c->props.visible || c->props.expand) continue;

        struct wlb_vec2 req_size;
        wlb_block_get_request_size(c, c->size_cache, &req_size);

        if (container->props.vertical) {
          size->x = fmaxf(size->x, req_size.x);
          size->y += req_size.y + container->props.margin;
        } else {
          size->x += req_size.x + container->props.margin;
          size->y = fmaxf(size->y, req_size.y);
        }
      }
    }
  }

  if (container->props.vertical) {
    size->x += container->props.margin * 2;
  } else {
    size->y += container->props.margin * 2;
  }
}

/* FIXME this entire function needs to be cleaned up */
static void arrange(struct wlb_block *block) {
  struct wlb_container *container = utl_container_of(block, container, block);

  if (utl_list_empty(&block->children)) return;

  int expand_cnt = 0;
  float nonexpand_size = 0;

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible) continue;
    if (c->props.expand) {
      expand_cnt++;
    } else {
      struct wlb_vec2 hint = {
          block->rect.w - 2 * container->props.margin,
          block->rect.h - 2 * container->props.margin,
      };
      struct wlb_vec2 req_size;
      wlb_block_get_request_size(c, hint, &req_size);
      float size = container->props.vertical ? req_size.y : req_size.x;
      nonexpand_size += size + container->props.margin;
    }
  }

  float alloc_size = (container->props.vertical ? block->rect.h : block->rect.w) -
                     container->props.margin;

  if (nonexpand_size > alloc_size) {
    reclaim(container, nonexpand_size - alloc_size);
    nonexpand_size = alloc_size;
  }

  float expanded_size = expand_cnt == 0
                            ? 0
                            : (alloc_size - nonexpand_size) /
                                      expand_cnt -
                                  container->props.margin;

  int adjusted = 0;
  if (expand_cnt > 0) {
    utl_list_for_each(c, &block->children, link) {
      if (!c->props.visible || !c->props.expand) continue;
      float min = container->props.vertical ? c->props.min_height : c->props.min_width;
      float max = container->props.vertical ? c->props.max_height : c->props.max_width;
      if (max > 0 && max < expanded_size) {
        expanded_size += (expanded_size - max) / (expand_cnt - ++adjusted);
      } else if (min > 0 && min > expanded_size) {
        expanded_size -= (min - expanded_size) / (expand_cnt - ++adjusted);
      }
    }
  }

  float p = container->props.margin;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible) continue;
    float min = container->props.vertical ? c->props.min_height : c->props.min_width;
    float max = container->props.vertical ? c->props.max_height : c->props.max_width;
    float size;
    if (c->props.expand) {
      size = expanded_size;
      if (min > 0 && size < min) size = min;
      if (max > 0 && size > max) size = max;
    } else {
      size = container->props.vertical ? c->size_cache.y : c->size_cache.x;
    }
    if (container->props.vertical) {
      wlb_block_apply_rect(c, (struct wlb_rectf){
                                  .x = block->rect.x + container->props.margin,
                                  .y = block->rect.y + p,
                                  .w = block->rect.w - container->props.margin * 2,
                                  .h = size,
                              });
      p += c->rect.h + container->props.margin;
    } else {
      wlb_block_apply_rect(c, (struct wlb_rectf){
                                  .x = block->rect.x + p,
                                  .y = block->rect.y + container->props.margin,
                                  .w = size,
                                  .h = block->rect.h - container->props.margin * 2,
                              });
      p += c->rect.w + container->props.margin;
    }
  }
}

static const struct wlb_block_impl impl = {
    .request_size = request_size,
    .arrange = arrange,
};

struct wlb_container *wlb_container_create(void) {
  struct wlb_container *container = calloc(1, sizeof(struct wlb_container));
  if (!container) return NULL;

  wlb_block_init(&container->block, &impl);

  container->props.vertical = true;
  container->props.margin = 0;

  wlb_signal_init(&container->events.sorted);

  return container;
}

bool wlb_container_set_vertical(struct wlb_container *container, bool vertical) {
  if (!container) return false;

  container->props.vertical = vertical;

  if (container->block.doc) container->block.doc->needs_refresh = true;

  return true;
}

bool wlb_container_set_margin(struct wlb_container *container, int margin) {
  if (!container) return false;

  container->props.margin = margin;

  if (container->block.doc) container->block.doc->needs_refresh = true;

  return true;
}
