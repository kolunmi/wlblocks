#include <math.h>
#include <rlgl.h>
#include <wlblocks/blocks.h>

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_label *label = utl_container_of(block, label, block);
  struct wlb_document *doc = label->block.doc;

  /* draw text on integers to avoid weird fuzziness */
  int x = roundf(block->rect.x + (label->block.rect.w - label->text_tex.width) / 2);
  int y = roundf(block->rect.y + (label->block.rect.h - label->text_tex.height) / 2);
  wlb_gl_texture_draw_easy(&label->text_tex, (struct wlb_vec2){x, y},
                           &doc->theme->color_config.normal.text);
}

static void destroy(struct wlb_block *block) {
  struct wlb_label *label = utl_container_of(block, label, block);

  if (label->text_tex.id) rlUnloadTexture(label->text_tex.id);
}

static const struct wlb_block_impl impl = {
    .draw = draw,
    .destroy = destroy,
};

struct wlb_label *wlb_label_create(void) {
  struct wlb_label *label = calloc(1, sizeof(struct wlb_label));
  if (!label) return NULL;

  wlb_block_init(&label->block, &impl);

  label->props.text = NULL;

  return label;
}

bool wlb_label_set_text(struct wlb_label *label, const char *text) {
  if (!label || !text) return false;

  struct wlb_document *doc = label->block.doc;

  struct wlb_texture tex;
  bool rv = wlb_gl_create_text_texture(&tex, &doc->theme->font, text,
                                       doc->scale, &(struct wlb_color){1, 1, 1, 1});
  if (!rv) return false;

  char *text_dup = strdup(text);
  if (!text_dup) {
    rlUnloadTexture(tex.id);
    return false;
  }

  if (label->text_tex.id) rlUnloadTexture(label->text_tex.id);
  label->text_tex = tex;

  free(label->props.text);
  label->props.text = text_dup;

  label->block.props.min_width = tex.width;
  label->block.props.min_height = tex.height;

  return true;
}
