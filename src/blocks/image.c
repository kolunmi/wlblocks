#include <math.h>
#include <rlgl.h>
#include <wlblocks/blocks.h>

static void request_size(struct wlb_block *block,
                         struct wlb_vec2 hint,
                         struct wlb_vec2 *size) {
  struct wlb_image *image = utl_container_of(block, image, block);
  if (!image->props.texture.id) return;

  struct wlb_rectf source = {0, 0, image->props.texture.width, image->props.texture.height};

  if (hint.x < 0 || hint.y < 0) {
    size->x = source.w;
    size->y = source.h;
    return;
  }

  float aspect = hint.x / hint.y;
  if (source.w > source.h) {
    float factor = hint.x / source.w;
    size->x = hint.x;
    size->y = source.h * factor * aspect;
  } else {
    float factor = hint.y / source.h;
    size->x = source.w * factor / aspect;
    size->y = hint.y;
  }
}

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_image *image = utl_container_of(block, image, block);
  if (!image->props.texture.id) return;

  struct wlb_rectf source = {0, 0, image->props.texture.width, image->props.texture.height};

  float w, h;
  if (source.h > source.w) {
    h = block->rect.h;
    w = (source.w / source.h) * h;
  } else {
    w = block->rect.w;
    h = (source.h / source.w) * w;
  }

  float x = block->rect.x + (block->rect.w - w) / 2;
  float y = block->rect.y + (block->rect.h - h) / 2;

  wlb_gl_texture_draw(&image->props.texture,
                      source,
                      (struct wlb_rectf){x, y, w, h},
                      &(struct wlb_color){1, 1, 1, 1});
}

static void destroy(struct wlb_block *block) {
  struct wlb_image *image = utl_container_of(block, image, block);

  if (image->props.texture.id) rlUnloadTexture(image->props.texture.id);
}

static const struct wlb_block_impl impl = {
    .request_size = request_size,
    .draw = draw,
    .destroy = destroy,
};

struct wlb_image *wlb_image_create(void) {
  struct wlb_image *image = calloc(1, sizeof(struct wlb_image));
  if (!image) return NULL;

  wlb_block_init(&image->block, &impl);

  image->props.texture = (struct wlb_texture){0};

  return image;
}

bool wlb_image_load_texture(struct wlb_image *image, struct wlb_texture *texture) {
  if (!image || !texture) return false;
  if (!texture->id) return false;

  image->props.texture = *texture;

  return true;
}
