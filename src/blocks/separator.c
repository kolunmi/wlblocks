#include <wlblocks/blocks.h>

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_separator *separator = utl_container_of(block, separator, block);
  struct wlb_document *doc = separator->block.doc;

  wlb_gl_draw_rect(block->rect, &doc->theme->color_config.disabled.border);
}

static const struct wlb_block_impl impl = {
    .draw = draw,
};

struct wlb_separator *wlb_separator_create(void) {
  struct wlb_separator *separator = calloc(1, sizeof(struct wlb_separator));
  if (!separator) return NULL;

  wlb_block_init(&separator->block, &impl);

  return separator;
}
