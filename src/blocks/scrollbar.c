#include <rlgl.h>
#include <wayland-client-protocol.h>
#include <wlblocks/blocks.h>

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_scrollbar *scrollbar = utl_container_of(block, scrollbar, block);
  struct wlb_document *doc = scrollbar->block.doc;

  wlb_gl_draw_rect(scrollbar->block.rect, &doc->theme->color_config.normal.base);

  float dim = scrollbar->props.horizontal ? block->rect.w : block->rect.h;
  float size = dim * scrollbar->props.ratio;
  float pos = (dim - size) * scrollbar->props.position;

  struct wlb_rectf drag_rect = {
      .x = scrollbar->props.horizontal ? block->rect.x + pos : block->rect.x,
      .y = scrollbar->props.horizontal ? block->rect.y : block->rect.y + pos,
      .w = scrollbar->props.horizontal ? size : block->rect.w,
      .h = scrollbar->props.horizontal ? block->rect.h : size,
  };

  wlb_gl_draw_rect(drag_rect,
                   block->hovered || block->dragging
                       ? &doc->theme->color_config.normal.text
                       : &doc->theme->color_config.normal.border);
}

static void get_pointer_priority(struct wlb_block *block, int *priority) {
  *priority = 0;
}

static void pointer_move(struct wlb_block *block, int x, int y, int dx, int dy) {
  struct wlb_scrollbar *scrollbar = utl_container_of(block, scrollbar, block);

  if (scrollbar->dragging) {
    float dim = scrollbar->props.horizontal ? block->rect.w : block->rect.h;
    float size = dim * scrollbar->props.ratio;
    float pos = ((scrollbar->props.horizontal ? x - block->rect.x : y - block->rect.y) - scrollbar->drag_offset) / (dim - size);

    wlb_scrollbar_set_position(scrollbar, pos);
  }
}

static void pointer_button(struct wlb_block *block,
                           int button, int state,
                           int x, int y) {
  struct wlb_scrollbar *scrollbar = utl_container_of(block, scrollbar, block);

  scrollbar->dragging = state == WL_POINTER_BUTTON_STATE_PRESSED;
  if (scrollbar->dragging) {
    float dim = scrollbar->props.horizontal ? block->rect.w : block->rect.h;
    float size = dim * scrollbar->props.ratio;
    float pos = (dim - size) * scrollbar->props.position +
                (scrollbar->props.horizontal ? block->rect.x : block->rect.y);
    int coord = scrollbar->props.horizontal ? x : y;
    float offset = coord - pos;

    scrollbar->drag_offset = offset >= 0 && offset < size ? offset : size / 2;
    pointer_move(block, x, y, 0, 0);
  }
}

static void destroy(struct wlb_block *block) {
  struct wlb_scrollbar *scrollbar = utl_container_of(block, scrollbar, block);
}

static const struct wlb_block_impl impl = {
    .draw = draw,
    .get_pointer_priority = get_pointer_priority,
    .pointer_move = pointer_move,
    .pointer_button = pointer_button,
    .destroy = destroy,
};

struct wlb_scrollbar *wlb_scrollbar_create(void) {
  struct wlb_scrollbar *scrollbar = calloc(1, sizeof(struct wlb_scrollbar));
  if (!scrollbar) return NULL;

  wlb_block_init(&scrollbar->block, &impl);

  wlb_signal_init(&scrollbar->events.scrolled);

  scrollbar->dragging = false;

  wlb_scrollbar_set_horizontal(scrollbar, false);

  return scrollbar;
}

bool wlb_scrollbar_set_horizontal(struct wlb_scrollbar *scrollbar,
                                  bool horizontal) {
  if (!scrollbar) return false;

  scrollbar->props.horizontal = horizontal;

  scrollbar->block.props.min_width = horizontal ? 0 : 25;
  scrollbar->block.props.max_width = horizontal ? 0 : 25;
  scrollbar->block.props.min_height = horizontal ? 25 : 0;
  scrollbar->block.props.max_height = horizontal ? 25 : 0;

  return true;
}

bool wlb_scrollbar_set_position(struct wlb_scrollbar *scrollbar, float position) {
  if (!scrollbar) return false;

  float adjusted = fminf(fmaxf(position, 0), 1);
  if (adjusted != scrollbar->props.position) {
    scrollbar->props.position = adjusted;
    wlb_signal_emit(&scrollbar->events.scrolled, NULL);
  }

  return true;
}

bool wlb_scrollbar_set_ratio(struct wlb_scrollbar *scrollbar, float ratio) {
  if (!scrollbar) return false;

  scrollbar->props.ratio = fminf(fmaxf(ratio, 0), 1);

  return true;
}
