#include <linux/input-event-codes.h>
#include <math.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>
#include <wlblocks/blocks.h>
#include <wlblocks/wlb_data.h>

static void request_size(struct wlb_block *block,
                         struct wlb_vec2 hint,
                         struct wlb_vec2 *size) {
  struct wlb_button *button = utl_container_of(block, button, block);

  struct wlb_texture icon_tex;
  struct wlb_rectf source;
  if (!wlb_global_get_icon(&icon_tex, &source, button->props.icon)) return;

  if (hint.x < 0) hint.x = source.w;
  if (hint.y < 0) hint.y = source.y;

  if (hint.y > hint.x) {
    size->x = fminf(source.w, hint.x);
    size->y = fminf((source.h / source.w) * size->x, hint.y);
  } else {
    size->y = fminf(source.h, hint.y);
    size->x = fminf((source.w / source.h) * size->y, hint.x);
  }
}

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_button *button = utl_container_of(block, button, block);
  struct wlb_document *doc = button->block.doc;

  struct wlb_texture icon_tex;
  struct wlb_rectf source;
  if (!wlb_global_get_icon(&icon_tex, &source, button->props.icon)) return;

  int w, h;
  if (block->rect.h > block->rect.w) {
    w = fminf(source.w, block->rect.w);
    h = fminf((source.h / source.w) * w, block->rect.h);
  } else {
    h = fminf(source.h, block->rect.h);
    w = fminf((source.w / source.h) * h, block->rect.w);
  }

  int x = block->rect.x + (block->rect.w - w) / 2;
  int y = block->rect.y + (block->rect.h - h) / 2;

  if (button->props.depth) {
    if (button->block.hovered) {
      x += 2;
      y += 2;
    }
    if (button->pressed) {
      x += 2;
      y += 2;
    }
  }

  wlb_gl_texture_draw(&icon_tex,
                      source,
                      (struct wlb_rectf){x, y, w, h},
                      &doc->theme->color_config.normal.text);
}

static void get_pointer_priority(struct wlb_block *block, int *priority) {
  *priority = 0;
}

static void pointer_button(struct wlb_block *block, int mouse_button, int state, int x, int y) {
  struct wlb_button *button = utl_container_of(block, button, block);

  if (mouse_button != BTN_LEFT) return;

  if (button->pressed && block->hovered &&
      state == WL_POINTER_BUTTON_STATE_RELEASED) {
    wlb_signal_emit(&button->events.pressed, NULL);
    button->pressed = false;
  }

  button->pressed = state == WL_POINTER_BUTTON_STATE_PRESSED;
}

static const struct wlb_block_impl impl = {
    .request_size = request_size,
    .draw = draw,
    .get_pointer_priority = get_pointer_priority,
    .pointer_button = pointer_button,
};

struct wlb_button *wlb_button_create(void) {
  struct wlb_button *button = calloc(1, sizeof(struct wlb_button));
  if (!button) return NULL;

  wlb_block_init(&button->block, &impl);

  wlb_signal_init(&button->events.pressed);

  button->props.icon = WLB_ICON_BUTTON;
  button->props.depth = true;

  return button;
}

bool wlb_button_set_icon(struct wlb_button *button, int icon) {
  if (!button || icon < 0) return false;

  button->props.icon = icon;

  return true;
}

bool wlb_button_set_depth(struct wlb_button *button, bool depth) {
  if (!button) return false;

  button->props.depth = depth;

  return true;
}
