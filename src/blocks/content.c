#include <wlblocks/blocks.h>

static void arrange(struct wlb_block *block) {
  struct wlb_content *content = utl_container_of(block, content, block);

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible) continue;

    struct wlb_vec2 req_size;
    wlb_block_get_request_size(c, (struct wlb_vec2){-1, -1}, &req_size);

    if (content->props.scale != 1) {
      req_size.x *= content->props.scale;
      req_size.y *= content->props.scale;
      wlb_block_get_request_size(c, req_size, &req_size);
    }

    wlb_block_apply_rect(c, (struct wlb_rectf){
                                .x = block->rect.x - content->props.offset.x,
                                .y = block->rect.y - content->props.offset.y,
                                .w = req_size.x,
                                .h = req_size.y,
                            });
  }
}

static const struct wlb_block_impl impl = {
    .arrange = arrange,
};

struct wlb_content *wlb_content_create(void) {
  struct wlb_content *content = calloc(1, sizeof(struct wlb_content));
  if (!content) return NULL;

  wlb_block_init(&content->block, &impl);

  content->props.scale = 1;

  return content;
}

bool wlb_content_set_offset(struct wlb_content *content,
                            struct wlb_vec2 offset) {
  if (!content) return false;

  content->props.offset = offset;

  if (content->block.doc) content->block.doc->needs_refresh = true;

  return true;
}

bool wlb_content_set_scale(struct wlb_content *content, float scale) {
  if (!content) return false;

  content->props.scale = scale;

  if (content->block.doc) content->block.doc->needs_refresh = true;

  return true;
}
