#include <math.h>
#include <rlgl.h>
#include <wlblocks/blocks.h>
#include <wlblocks/crdraw.h>
#include <wlblocks/gldraw.h>
#include <wlblocks/wlb_data.h>

static void request_size(struct wlb_block *block,
                         struct wlb_vec2 hint,
                         struct wlb_vec2 *size) {
  struct wlb_spinner *spinner = utl_container_of(block, spinner, block);

  struct wlb_texture icon_tex;
  struct wlb_rectf source;
  if (!wlb_global_get_icon(&icon_tex, &source, WLB_ICON_GEAR)) return;

  if (hint.x < 0 || hint.y < 0) {
    size->x = source.w;
    size->y = source.h;
    return;
  }

  if (hint.y > hint.x) {
    size->x = fmin(source.w, hint.x);
    size->y = fmin((source.h / (float)source.w) * size->x, hint.y);
  } else {
    size->y = fmin(source.h, hint.y);
    size->x = fmin((source.w / (float)source.h) * size->y, hint.x);
  }
}

static void draw(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  struct wlb_spinner *spinner = utl_container_of(block, spinner, block);
  struct wlb_document *doc = spinner->block.doc;

  struct wlb_texture icon_tex;
  struct wlb_rectf source;
  if (!wlb_global_get_icon(&icon_tex, &source, WLB_ICON_GEAR)) return;

  int w, h;
  if (block->rect.h > block->rect.w) {
    w = fmin(source.w, block->rect.w);
    h = fmin((source.h / (float)source.w) * w, block->rect.h);
  } else {
    h = fmin(source.h, block->rect.h);
    w = fmin((source.w / (float)source.h) * h, block->rect.w);
  }

  int x = block->rect.x + block->rect.w / 2;
  int y = block->rect.y + block->rect.h / 2;
  float cx = w / 2.0;
  float cy = h / 2.0;

  wlb_gl_texture_draw_extended(&icon_tex,
                               source,
                               (struct wlb_rectf){x, y, w, h},
                               &doc->theme->color_config.normal.text,
                               (struct wlb_vec2){cx, cy},
                               spinner->rotation);
  float delta = doc->delta.tv_nsec * 0.0000000025;
  spinner->rotation += spinner->props.clockwise ? delta : -delta;
}

static const struct wlb_block_impl impl = {
    .request_size = request_size,
    .draw = draw,
};

struct wlb_spinner *wlb_spinner_create(void) {
  struct wlb_spinner *spinner = calloc(1, sizeof(struct wlb_spinner));
  if (!spinner) return NULL;

  wlb_block_init(&spinner->block, &impl);

  spinner->props.clockwise = true;

  return spinner;
}

bool wlb_spinner_set_clockwise(struct wlb_spinner *spinner, bool clockwise) {
  if (!spinner) return false;

  spinner->props.clockwise = clockwise;

  return true;
}
