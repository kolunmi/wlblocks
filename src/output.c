#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client-protocol.h>
#include <wlblocks/output.h>
#include <wlblocks/log.h>

static void output_name(void *data, struct wl_output *wl_output, const char *name);
static void output_description(void *data, struct wl_output *wl_output, const char *description);
static void output_scale(void *data, struct wl_output *wl_output, int32_t factor);
static void output_mode(void *data, struct wl_output *wl_output, uint32_t flags, int32_t width, int32_t height, int32_t refresh);
static void output_geometry(void *data, struct wl_output *wl_output, int32_t x, int32_t y, int32_t physical_width, int32_t physical_height, int32_t subpixel, const char *make, const char *model, int32_t transform);
static void output_done(void *data, struct wl_output *wl_output);

static const struct wl_output_listener output_listener = {
    .name = output_name,
    .description = output_description,
    .scale = output_scale,
    .mode = output_mode,
    .geometry = output_geometry,
    .done = output_done,
};

void output_name(void *data, struct wl_output *wl_output, const char *name) {
    struct wlb_output *output = data;
    free(output->name);
    output->name = strdup(name);
}

void output_description(void *data, struct wl_output *wl_output, const char *description) { }

void output_scale(void *data, struct wl_output *wl_output, int32_t factor) {
    struct wlb_output *output = data;
    output->scale = factor;
}

void output_mode(void *data, struct wl_output *wl_output, uint32_t flags, int32_t width, int32_t height, int32_t refresh) {
    struct wlb_output *output = data;
    output->refresh = refresh;
}

void output_geometry(void *data, struct wl_output *wl_output, int32_t x, int32_t y, int32_t physical_width, int32_t physical_height, int32_t subpixel, const char *make, const char *model, int32_t transform) {
    struct wlb_output *output = data;
    output->subpixel = subpixel;
    output->transform = transform;
}

void output_done(void *data, struct wl_output *wl_output) { }

bool wlb_output_init(struct wlb_output *output, uint32_t name, struct wl_output *wl_output) {
    if (!output || !wl_output) return false;

    memset(output, 0, sizeof(*output));

    output->wl_name = name;
    output->output = wl_output;

    if (wl_output_add_listener(wl_output, &output_listener, output) == -1) {
        wlb_log(WLB_LOG_ERROR, "failed to add wl_output listener");
        return false;
    }

    return true;
}

bool wlb_output_destroy(struct wlb_output *output) {
    if (!output) return false;

    wl_output_destroy(output->output);
    free(output->name);

    return true;
}
