#include <stdint.h>
#include <stdio.h>

#include <wlblocks/log.h>

#define GRAPHICS_API_OPENGL_ES2
#define TRACELOG(level, ...) wlb_log(WLB_LOG_DEBUG, __VA_ARGS__)
#define TRACELOGD(...) wlb_log(WLB_LOG_DEBUG, __VA_ARGS__)
#define RLGL_IMPLEMENTATION
#include <rlgl.h>

#include <wlblocks/crdraw.h>
#include <wlblocks/gldraw.h>
#include <wlblocks/log.h>

bool wlb_gl_make_viewport(int width, int height) {
  rlViewport(0, 0, width, height);
  rlMatrixMode(RL_PROJECTION);
  rlLoadIdentity();
  rlOrtho(0, width, height, 0, 0, 1);
  rlMatrixMode(RL_MODELVIEW);
  rlLoadIdentity();

  return true;
}

bool wlb_gl_texture_load(struct wlb_texture *dest,
                         struct wlb_data data,
                         int width, int height,
                         int mipmaps, int format) {
  if (!dest || width <= 0 || height <= 0) return false;

  dest->id = rlLoadTexture(data.ptr, width, height, format, mipmaps);
  if (!dest->id) return false;

  dest->width = width;
  dest->height = height;
  dest->mipmaps = mipmaps;
  dest->format = format;

  rlTextureParameters(dest->id, RL_TEXTURE_MIN_FILTER, RL_TEXTURE_FILTER_LINEAR);

  wlb_log(WLB_LOG_DEBUG, "loaded texture with pixel dimensions %dx%d", width, height);

  return true;
}

bool wlb_gl_texture_from_cairo_surface(struct wlb_texture *dest, cairo_surface_t *surface) {
  if (!dest || !surface) return false;

  size_t w = cairo_image_surface_get_width(surface);
  size_t h = cairo_image_surface_get_height(surface);
  size_t len = w * h * 4;
  char *rgba = malloc(len);
  if (!rgba) return false;

  unsigned char *surface_data = cairo_image_surface_get_data(surface);

  size_t i;
  for (i = 0; i < len; i += 4) {
    rgba[i + 0] = surface_data[i + 2];
    rgba[i + 1] = surface_data[i + 1];
    rgba[i + 2] = surface_data[i + 0];
    rgba[i + 3] = surface_data[i + 3];
  }

  struct wlb_data data = {
      .ptr = (uint8_t *)rgba,
      .size = len,
  };

  bool rv = wlb_gl_texture_load(dest, data, w, h, 1,
                                RL_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
  free(rgba);

  return rv;
}

bool wlb_gl_create_text_texture(struct wlb_texture *dest,
                                struct wlb_font *font,
                                const char *text,
                                double scale,
                                struct wlb_color *fg) {
  if (!dest || !font || !text || !fg) return false;

  cairo_surface_t *surface = wlb_cairo_create_text_buffer(font, text, scale, fg);
  if (!surface) return false;

  bool rv = wlb_gl_texture_from_cairo_surface(dest, surface);
  cairo_surface_destroy(surface);

  return rv;
}

bool wlb_gl_create_svg_texture(struct wlb_texture *dest,
                               struct wlb_data data,
                               int width, int height) {
  if (!dest || !WLB_DATA_IS_VALID(&data)) return false;

  cairo_surface_t *surface = wlb_cairo_create_svg_buffer(data, width, height);
  if (!surface) return false;

  bool rv = wlb_gl_texture_from_cairo_surface(dest, surface);
  cairo_surface_destroy(surface);

  return rv;
}

bool wlb_gl_texture_draw(struct wlb_texture *texture,
                         struct wlb_rectf source,
                         struct wlb_rectf dest,
                         struct wlb_color *tint) {
  return wlb_gl_texture_draw_extended(texture, source, dest, tint,
                                      (struct wlb_vec2){0, 0}, 0);
}

bool wlb_gl_texture_draw_extended(struct wlb_texture *texture,
                                  struct wlb_rectf source,
                                  struct wlb_rectf dest,
                                  struct wlb_color *tint,
                                  struct wlb_vec2 origin,
                                  float rotation) {
  if (!texture || !tint) return false;

  float width = texture->width;
  float height = texture->height;

  bool flip_x = false;
  if (source.w < 0) {
    flip_x = true;
    source.w *= -1;
  }
  if (source.h < 0) source.y -= source.h;

  struct wlb_vec2 top_left;
  struct wlb_vec2 top_right;
  struct wlb_vec2 bottom_left;
  struct wlb_vec2 bottom_right;

  if (rotation == 0) {
    top_left = (struct wlb_vec2){dest.x, dest.y};
    top_right = (struct wlb_vec2){dest.x + dest.w, dest.y};
    bottom_left = (struct wlb_vec2){dest.x, dest.y + dest.h};
    bottom_right = (struct wlb_vec2){dest.x + dest.w, dest.y + dest.h};
  } else {
    float sin_rot = sinf(rotation);
    float cos_rot = cosf(rotation);
    float x = dest.x;
    float y = dest.y;
    float dx = -origin.x;
    float dy = -origin.y;

    top_left.x = x + dx * cos_rot - dy * sin_rot;
    top_left.y = y + dx * sin_rot + dy * cos_rot;

    top_right.x = x + (dx + dest.w) * cos_rot - dy * sin_rot;
    top_right.y = y + (dx + dest.w) * sin_rot + dy * cos_rot;

    bottom_left.x = x + dx * cos_rot - (dy + dest.h) * sin_rot;
    bottom_left.y = y + dx * sin_rot + (dy + dest.h) * cos_rot;

    bottom_right.x = x + (dx + dest.w) * cos_rot - (dy + dest.h) * sin_rot;
    bottom_right.y = y + (dx + dest.w) * sin_rot + (dy + dest.h) * cos_rot;
  }

  rlSetTexture(texture->id);
  rlBegin(RL_QUADS);

  rlColor4ub(tint->r * 255, tint->g * 255, tint->b * 255, tint->a * 255);
  rlNormal3f(0.0f, 0.0f, 1.0f);

  if (flip_x) {
    rlTexCoord2f((source.x + source.w) / width, source.y / height);
  } else {
    rlTexCoord2f(source.x / width, source.y / height);
  }
  rlVertex2f(top_left.x, top_left.y);

  if (flip_x) {
    rlTexCoord2f((source.x + source.w) / width, (source.y + source.h) / height);
  } else {
    rlTexCoord2f(source.x / width, (source.y + source.h) / height);
  }
  rlVertex2f(bottom_left.x, bottom_left.y);

  if (flip_x) {
    rlTexCoord2f(source.x / width, (source.y + source.h) / height);
  } else {
    rlTexCoord2f((source.x + source.w) / width, (source.y + source.h) / height);
  }
  rlVertex2f(bottom_right.x, bottom_right.y);

  if (flip_x) {
    rlTexCoord2f(source.x / width, source.y / height);
  } else {
    rlTexCoord2f((source.x + source.w) / width, source.y / height);
  }
  rlVertex2f(top_right.x, top_right.y);

  rlEnd();
  rlSetTexture(0);

  return true;
}

bool wlb_gl_texture_draw_easy(struct wlb_texture *texture, struct wlb_vec2 position, struct wlb_color *tint) {
  struct wlb_rectf source = {0, 0, texture->width, texture->height};
  struct wlb_rectf dest = {position.x, position.y, texture->width, texture->height};

  return wlb_gl_texture_draw(texture, source, dest, tint);
}

bool wlb_gl_render_texture_load(struct wlb_render_texture *dest, int width, int height) {
  if (!dest || width <= 0 || height <= 0) return false;

  dest->id = rlLoadFramebuffer();
  if (!dest->id) return false;

  bool ret = false;
  rlEnableFramebuffer(dest->id);

  dest->texture.id = rlLoadTexture(NULL, width, height, RL_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8, 1);
  if (!dest->texture.id) goto err_load_tex;
  dest->texture.width = width;
  dest->texture.height = height;
  dest->texture.format = RL_PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;
  dest->texture.mipmaps = 1;

  dest->depth.id = rlLoadTextureDepth(width, height, true);
  if (!dest->depth.id) goto err_load_tex_depth;
  dest->depth.width = width;
  dest->depth.height = height;
  dest->depth.format = 19; // DEPTH_COMPONENT_24BIT?
  dest->depth.mipmaps = 1;

  rlFramebufferAttach(dest->id, dest->texture.id, RL_ATTACHMENT_COLOR_CHANNEL0, RL_ATTACHMENT_TEXTURE2D, 0);
  rlFramebufferAttach(dest->id, dest->depth.id, RL_ATTACHMENT_DEPTH, RL_ATTACHMENT_RENDERBUFFER, 0);
  if (!rlFramebufferComplete(dest->id)) goto err_framebuffer;

  ret = true;
  goto finish;

err_framebuffer:
  rlUnloadTexture(dest->depth.id);
err_load_tex_depth:
  rlUnloadTexture(dest->texture.id);
err_load_tex:
  rlUnloadFramebuffer(dest->id);

finish:
  rlDisableFramebuffer();

  return ret;
}

bool wlb_gl_begin_texture(struct wlb_render_texture *rtexture) {
  if (!rtexture) return false;

  rlDrawRenderBatchActive();

  rlEnableFramebuffer(rtexture->id);

  rlViewport(0, 0, rtexture->texture.width, rtexture->texture.height);
  rlSetFramebufferWidth(rtexture->texture.width);
  rlSetFramebufferHeight(rtexture->texture.height);

  rlMatrixMode(RL_PROJECTION);
  rlLoadIdentity();

  rlOrtho(0, rtexture->texture.width, rtexture->texture.height, 0, 0.0f, 1.0f);

  rlMatrixMode(RL_MODELVIEW);
  rlLoadIdentity();

  /* rlScalef(0.0f, -1.0f, 0.0f); */

  return true;
}

bool wlb_gl_end_texture(int width, int height) {
  rlDrawRenderBatchActive();
  rlDisableFramebuffer();

  wlb_gl_make_viewport(width, height);
  rlSetFramebufferWidth(width);
  rlSetFramebufferHeight(height);

  rlMatrixMode(RL_MODELVIEW);
  rlLoadIdentity();

  return true;
}

bool wlb_gl_draw_rect(struct wlb_rectf rect, struct wlb_color *color) {
  struct wlb_vec2 top_left = {rect.x, rect.y};
  struct wlb_vec2 top_right = {rect.x + rect.w, rect.y};
  struct wlb_vec2 bottom_left = {rect.x, rect.y + rect.h};
  struct wlb_vec2 bottom_right = {rect.x + rect.w, rect.y + rect.h};

  rlSetTexture(rlGetTextureIdDefault());

  rlBegin(RL_QUADS);

  rlNormal3f(0.0f, 0.0f, 1.0f);
  rlColor4ub(color->r * 255, color->g * 255, color->b * 255, color->a * 255);

  rlTexCoord2f(0, 0);
  rlVertex2f(top_left.x, top_left.y);

  rlTexCoord2f(0, 1);
  rlVertex2f(bottom_left.x, bottom_left.y);

  rlTexCoord2f(1, 1);
  rlVertex2f(bottom_right.x, bottom_right.y);

  rlTexCoord2f(1, 0);
  rlVertex2f(top_right.x, top_right.y);

  rlEnd();

  rlSetTexture(0);

  return true;
}
