#include <math.h>
#include <wlblocks/basic.h>

bool wlb_rect_empty(struct wlb_rect *rect) {
  return rect == NULL || rect->w <= 0 || rect->h <= 0;
}

bool wlb_rect_intersection(struct wlb_rect *dest,
                           struct wlb_rect *rect_a,
                           struct wlb_rect *rect_b) {
  bool a_empty = wlb_rect_empty(rect_a);
  bool b_empty = wlb_rect_empty(rect_b);

  if (a_empty || b_empty) {
    *dest = (struct wlb_rect){0};
    return false;
  }

  int x1 = fmaxf(rect_a->x, rect_b->x);
  int y1 = fmaxf(rect_a->y, rect_b->y);
  int x2 = fminf(rect_a->x + rect_a->w, rect_b->x + rect_b->w);
  int y2 = fminf(rect_a->y + rect_a->h, rect_b->y + rect_b->h);

  dest->x = x1;
  dest->y = y1;
  dest->w = x2 - x1;
  dest->h = y2 - y1;

  return !wlb_rect_empty(dest);
}

bool wlb_rectf_empty(struct wlb_rectf *rect) {
  return rect == NULL || rect->w <= 0 || rect->h <= 0;
}

bool wlb_rectf_closest_point(struct wlb_rectf *rect, float x, float y,
                             float *dest_x, float *dest_y) {
  if (wlb_rectf_empty(rect)) return false;
  if (!dest_x || !dest_y) return false;

  if (x < rect->x) {
    *dest_x = rect->x;
  } else if (x >= rect->x + rect->w) {
    *dest_x = rect->x + rect->w - 1;
  } else {
    *dest_x = x;
  }

  if (y < rect->y) {
    *dest_y = rect->y;
  } else if (y >= rect->y + rect->h) {
    *dest_y = rect->y + rect->h - 1;
  } else {
    *dest_y = y;
  }

  return true;
}

bool wlb_rectf_intersection(struct wlb_rectf *dest,
                            struct wlb_rectf *rect_a,
                            struct wlb_rectf *rect_b) {
  bool a_empty = wlb_rectf_empty(rect_a);
  bool b_empty = wlb_rectf_empty(rect_b);

  if (a_empty || b_empty) {
    *dest = (struct wlb_rectf){0};
    return false;
  }

  int x1 = fmaxf(rect_a->x, rect_b->x);
  int y1 = fmaxf(rect_a->y, rect_b->y);
  int x2 = fminf(rect_a->x + rect_a->w, rect_b->x + rect_b->w);
  int y2 = fminf(rect_a->y + rect_a->h, rect_b->y + rect_b->h);

  dest->x = x1;
  dest->y = y1;
  dest->w = x2 - x1;
  dest->h = y2 - y1;

  return !wlb_rectf_empty(dest);
}
