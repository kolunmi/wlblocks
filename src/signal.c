#include <utl/vector.h>
#include <stdint.h>
#include <wayland-util.h>
#include <wlblocks/signal.h>

bool wlb_signal_init(struct wlb_signal *signal) {
    if (!signal) return false;

    utl_list_init(&signal->listeners);

    return true;
}

bool wlb_signal_add(struct wlb_signal *signal, struct wlb_listener *listener) {
    if (!signal || !listener || !listener->notify) return false;

    utl_list_insert(&signal->listeners, &listener->link);

    return true;
}

bool wlb_signal_emit(struct wlb_signal *signal, void *data) {
    if (!signal) return false;

    struct wlb_listener cursor, end;

    utl_list_insert(&signal->listeners, &cursor.link);
    utl_list_insert(signal->listeners.prev, &end.link);

    while (cursor.link.next != &end.link) {
        struct utl_list *pos = cursor.link.next;
        struct wlb_listener *listener = wl_container_of(pos, listener, link);

        utl_list_remove(&cursor.link);
        utl_list_insert(pos, &cursor.link);

        listener->notify(listener, data);
    }

    utl_list_remove(&cursor.link);
    utl_list_remove(&end.link);

    return true;
}

bool wlb_listener_remove(struct wlb_listener *listener) {
    if (!listener) return false;
    utl_list_remove(&listener->link);
    return true;
}
