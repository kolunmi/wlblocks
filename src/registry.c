#include <wlblocks/log.h>
#include <utl/vector.h>
#include <stdint.h>
#include <stdio.h>
#include <wayland-client-core.h>
#include <wlblocks/signal.h>
#include <utl/hash_map.h>
#include <utl/common.h>
#include <utl/hash_table.h>
#include <stdlib.h>
#include <wayland-client-protocol.h>
#include <wlblocks/registry.h>

static bool wlb_registry_announce_globals(const void *data, void *user_data);
static void wlb_registry_interface_destroy(void *data);
static void wlb_registry_global_destroy(void *data);
static void global_add(void *data, struct wl_registry *wl_registry, uint32_t name, const char *interface, uint32_t version);
static void global_remove(void *data, struct wl_registry *wl_registry, uint32_t name);

static const struct wl_registry_listener registry_listener = {
    .global = global_add,
    .global_remove = global_remove,
};

void global_add(void *data, struct wl_registry *wl_registry, uint32_t name, const char *wl_interface, uint32_t version) {
    wlb_log(WLB_LOG_DEBUG, "%d %s %d", name, wl_interface, version);
    struct wlb_registry *registry = data;
    struct wlb_registry_interface *interface = utl_hash_map_get(&registry->interfaces, wl_interface);
    if (!interface || utl_hash_map_get(&registry->globals, &name)) return;
    if (interface->version > version)  {
        wlb_log(WLB_LOG_WARNING, "global interface version mismatch for %s, wanted: %d, have: %d", interface->wl_interface->name, interface->version, version);
        return;
    }

    struct wlb_registry_global *global = calloc(1, sizeof(*global));
    if (!global) return;

    global->name = name;
    global->interface = interface;
    global->global = wl_registry_bind(wl_registry, name, interface->wl_interface, interface->version ? interface->version : version);
    if (!utl_hash_map_insert(&registry->globals, &name, global)) goto map_err;
    if (!utl_vector_push(&interface->globals, &name)) goto vec_err;

    if (!registry->registered) return;
    wlb_signal_emit(&interface->signals.add, &global);

    return;
vec_err:
    utl_hash_map_remove(&registry->globals, &name);
map_err:
    free(global);
}

void global_remove(void *data, struct wl_registry *wl_registry, uint32_t name) {
    struct wlb_registry *registry = data;
    struct wlb_registry_global *global = utl_hash_map_get(&registry->globals, &name);
    if (!global || !wlb_signal_emit(&global->interface->signals.remove, global)) return;
    free(utl_hash_map_get(&registry->globals, &name));
}

bool wlb_registry_announce_globals(const void *data, void *user_data) {
    const struct wlb_registry_global *global = data;
    wlb_signal_emit(&global->interface->signals.add, (void*)global);
    return false;
}

void wlb_registry_interface_destroy(void *data) {
    struct wlb_registry_interface *interface = data;
    utl_vector_destroy(&interface->globals);
    free(interface);
}

void wlb_registry_global_destroy(void *data) {
    struct wlb_registry_global *global = data;
    wlb_signal_emit(&global->interface->signals.remove, global);
    free(global);
}

bool wlb_registry_init(struct wlb_registry *registry) {
    if (!registry) return false;

    if (!utl_hash_map_init(&registry->globals, utl_murmur3_uint32_t_hash)) return false;
    if (!utl_hash_map_init(&registry->interfaces, utl_murmur3_string_hash)) return false;

    return true;
}

bool wlb_registry_destroy(struct wlb_registry *registry) {
    if (!registry) return false;

    if (!utl_hash_map_elements_destroy(&registry->globals, wlb_registry_global_destroy)) return false;
    if (!utl_hash_map_elements_destroy(&registry->interfaces, wlb_registry_interface_destroy)) return false;

    return true;
}

bool wlb_registry_elements_destroy(struct wlb_registry *registry, utl_iterator_func iterator, void *data) {
    if (!registry || !iterator) return false;

    if (!utl_hash_map_iterate(&registry->globals, data, iterator)) return false;

    return true;
}

bool wlb_registry_register(struct wlb_registry *registry, struct wl_display *display, struct wl_registry *wl_registry) {
    if (!registry || !display || !wl_registry) return false;

    if (wl_registry_add_listener(wl_registry, &registry_listener, registry) == -1) return false;
    if (wl_display_roundtrip(display) == -1) return false;

    registry->registered = true;

    return utl_hash_map_iterate(&registry->globals, NULL, wlb_registry_announce_globals);
}

struct wlb_registry_interface *wlb_registry_add_interface(struct wlb_registry *registry, const struct wl_interface *wl_interface, uint32_t version, struct wlb_listener *add, struct wlb_listener *remove) {
    if (!registry || !wl_interface || utl_hash_map_get(&registry->interfaces, wl_interface->name)) return NULL;

    struct wlb_registry_interface *interface = calloc(1, sizeof(*interface));
    if (!interface) return NULL;

    utl_hash_map_insert(&registry->interfaces, wl_interface->name, interface);

    interface->wl_interface = wl_interface;
    interface->version = version;
    if (!utl_vector_init(&interface->globals, sizeof(uint32_t))) goto err;
    if (!wlb_signal_init(&interface->signals.add)) goto err;
    if (!wlb_signal_init(&interface->signals.remove)) goto err;
    if (add && !wlb_signal_add(&interface->signals.add, add)) return false;
    if (remove && !wlb_signal_add(&interface->signals.remove, remove)) return false;

    return interface;

err:
    free(interface);
    return NULL;
}

struct wlb_registry_interface *wlb_registry_get_interface(struct wlb_registry *registry, const struct wl_interface *interface) {
    if (!registry || interface) return NULL;

    return utl_hash_map_get(&registry->interfaces, interface->name);
}

struct wlb_registry_global *wlb_registry_get_global(struct wlb_registry *registry, uint32_t name) {
    if (!registry) return NULL;

    return utl_hash_map_get(&registry->globals, &name);
}

struct wlb_registry_global *wlb_registry_get_single(struct wlb_registry *registry, const struct wl_interface *wl_interface) {
    if (!registry || !wl_interface) return NULL;

    struct wlb_registry_interface *interface = utl_hash_map_get(&registry->interfaces, wl_interface->name);
    if (!interface || interface->globals.length == 0) return NULL;

    return utl_hash_map_get(&registry->globals, utl_vector_get(&interface->globals, 0));
}
