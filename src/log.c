#include <stdarg.h>
#include <stdio.h>
#include <wlblocks/log.h>

static void stderr_log(enum wlb_log_level level, const char *fmt, va_list ap);

static wlb_log_func log_func = stderr_log;
static enum wlb_log_level log_level = WLB_LOG_SILENT;
static struct timespec start = {0};
static const char *log_levels[] = {
    [WLB_LOG_SILENT] = "",
    [WLB_LOG_ERROR] = "[error]",
    [WLB_LOG_WARNING] = "[warning]",
    [WLB_LOG_INFO] = "[info]",
    [WLB_LOG_DEBUG] = "[debug]",
};

void timespec_sub(struct timespec *result, struct timespec *left, struct timespec *right) {
  if (!result || !left || !right) return;

  result->tv_sec = left->tv_sec - right->tv_sec;
  result->tv_nsec = left->tv_nsec - right->tv_nsec;
  if (result->tv_nsec < 0) {
    result->tv_sec--;
    result->tv_nsec += 1000000000;
  }
}

void stderr_log(enum wlb_log_level level, const char *fmt, va_list ap) {
  struct timespec current = {0};
  clock_gettime(CLOCK_MONOTONIC, &current);
  timespec_sub(&current, &current, &start);

  fprintf(stderr, "[%02d:%02d:%02d:%03ld][libwlblocks][%s]",
          (int)(current.tv_sec / 60 / 60),   // Hours
          (int)((current.tv_sec / 60) % 60), // Minutes
          (int)(current.tv_sec % 60),        // Seconds
          current.tv_nsec / 1000000,         // Milliseconds
          log_levels[level]);

  vfprintf(stderr, fmt, ap);

  fprintf(stderr, "\n");
}

bool wlb_log_init(enum wlb_log_level level, wlb_log_func func) {
  log_func = func ? func : stderr_log;
  log_level = level;

  if (clock_gettime(CLOCK_MONOTONIC, &start) == -1) return false;

  return false;
}

void _wlb_logv(enum wlb_log_level level, const char *fmt, va_list ap) {
  if (level > log_level) return;
  log_func(level, fmt, ap);
}

void _wlb_log(enum wlb_log_level level, const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  _wlb_logv(level, fmt, ap);
  va_end(ap);
}
