#include <wlblocks/crdraw.h>
#include <wlblocks/wlb_data.h>

static struct wlb_texture icons_texture;
static int icons_texture_x_dim;

bool wlb_global_init_resources(void) {
  struct wlb_data icons[] = {
      {_wlbdata_add, _wlbdata_add_len},
      {_wlbdata_arrow_down, _wlbdata_arrow_down_len},
      {_wlbdata_arrow_left, _wlbdata_arrow_left_len},
      {_wlbdata_arrow_right, _wlbdata_arrow_right_len},
      {_wlbdata_arrow_up, _wlbdata_arrow_up_len},
      {_wlbdata_bin, _wlbdata_bin_len},
      {_wlbdata_burger, _wlbdata_burger_len},
      {_wlbdata_button_pressed, _wlbdata_button_pressed_len},
      {_wlbdata_button, _wlbdata_button_len},
      {_wlbdata_check, _wlbdata_check_len},
      {_wlbdata_clipboard, _wlbdata_clipboard_len},
      {_wlbdata_close, _wlbdata_close_len},
      {_wlbdata_copy, _wlbdata_copy_len},
      {_wlbdata_download, _wlbdata_download_len},
      {_wlbdata_down, _wlbdata_down_len},
      {_wlbdata_file_copy, _wlbdata_file_copy_len},
      {_wlbdata_file_cross, _wlbdata_file_cross_len},
      {_wlbdata_file, _wlbdata_file_len},
      {_wlbdata_folder_cross, _wlbdata_folder_cross_len},
      {_wlbdata_folder, _wlbdata_folder_len},
      {_wlbdata_fold, _wlbdata_fold_len},
      {_wlbdata_gear, _wlbdata_gear_len},
      {_wlbdata_grid, _wlbdata_grid_len},
      {_wlbdata_left, _wlbdata_left_len},
      {_wlbdata_magnifier, _wlbdata_magnifier_len},
      {_wlbdata_maximize, _wlbdata_maximize_len},
      {_wlbdata_minimize, _wlbdata_minimize_len},
      {_wlbdata_new_file, _wlbdata_new_file_len},
      {_wlbdata_new_folder, _wlbdata_new_folder_len},
      {_wlbdata_off, _wlbdata_off_len},
      {_wlbdata_on, _wlbdata_on_len},
      {_wlbdata_options, _wlbdata_options_len},
      {_wlbdata_paste, _wlbdata_paste_len},
      {_wlbdata_pause, _wlbdata_pause_len},
      {_wlbdata_pen, _wlbdata_pen_len},
      {_wlbdata_play, _wlbdata_play_len},
      {_wlbdata_redo, _wlbdata_redo_len},
      {_wlbdata_restore, _wlbdata_restore_len},
      {_wlbdata_right, _wlbdata_right_len},
      {_wlbdata_scissor, _wlbdata_scissor_len},
      {_wlbdata_search, _wlbdata_search_len},
      {_wlbdata_settings, _wlbdata_settings_len},
      {_wlbdata_stop, _wlbdata_stop_len},
      {_wlbdata_undo, _wlbdata_undo_len},
      {_wlbdata_upload, _wlbdata_upload_len},
      {_wlbdata_up, _wlbdata_up_len},
      {_wlbdata_windows, _wlbdata_windows_len},
      {_wlbdata_zip, _wlbdata_zip_len},
  };

  cairo_surface_t *surface = wlb_cairo_create_batch_svg_buffer(icons,
                                                               sizeof(icons) / sizeof(icons[0]),
                                                               128, &icons_texture_x_dim);
  if (!surface) return false;

  /* cairo_surface_write_to_png(surface, "icons.png"); */

  bool rv = wlb_gl_texture_from_cairo_surface(&icons_texture, surface);
  cairo_surface_destroy(surface);
  if (!rv) return false;

  return true;
}

bool wlb_global_get_icon(struct wlb_texture *dest,
                         struct wlb_rectf *rect,
                         enum wlbdata_icon icon) {
  if (!dest || icon < 0 || icon >= _WLB_ICON_MAX) return false;

  *dest = icons_texture;
  *rect = (struct wlb_rectf){
      .x = (int)((icon % icons_texture_x_dim) * 128),
      .y = (int)((icon / icons_texture_x_dim) * 128),
      .w = 128,
      .h = 128,
  };

  return true;
}
