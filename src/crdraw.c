#include <librsvg/rsvg.h>
#include <math.h>
#include <string.h>
#include <wlblocks/crdraw.h>

PangoLayout *wlb_cairo_get_pango_layout(cairo_t *cr,
                                        struct wlb_font *font,
                                        const char *text,
                                        double scale,
                                        bool markup) {
  if (!cr || !font || !text) return NULL;

  PangoLayout *layout = pango_cairo_create_layout(cr);
  if (!layout) return NULL;

  PangoAttrList *attrs;
  if (markup) {
    char *buf;
    GError *error = NULL;
    if (pango_parse_markup(text, -1, 0, &attrs, &buf, NULL, &error)) {
      pango_layout_set_text(layout, buf, -1);
      free(buf);
    } else {
      g_error_free(error);
      markup = false;
    }
  }
  if (!markup) {
    if (!(attrs = pango_attr_list_new())) {
      g_object_unref(layout);
      return NULL;
    }
    pango_layout_set_text(layout, text, -1);
  }

  pango_attr_list_insert(attrs, pango_attr_scale_new(scale));
  pango_layout_set_font_description(layout, font->desc);
  pango_layout_set_single_paragraph_mode(layout, 1);
  pango_layout_set_attributes(layout, attrs);
  pango_attr_list_unref(attrs);

  return layout;
}

bool wlb_cairo_draw_text(cairo_t *cr,
                         struct wlb_font *font,
                         const char *text,
                         struct wlb_color *foreground,
                         struct wlb_color *background,
                         double scale,
                         bool markup,
                         int *width,
                         int *height) {
  if (!cr || !font || !text) return false;

  PangoLayout *layout = wlb_cairo_get_pango_layout(cr, font, text, scale, markup);
  if (!layout) return false;

  cairo_font_options_t *font_options = cairo_font_options_create();
  if (!font_options) {
    g_object_unref(layout);
    return false;
  }

  cairo_get_font_options(cr, font_options);
  cairo_font_options_set_antialias(font_options, CAIRO_ANTIALIAS_GRAY);
  pango_cairo_context_set_font_options(pango_layout_get_context(layout),
                                       font_options);
  cairo_font_options_destroy(font_options);
  pango_cairo_update_layout(cr, layout);

  double x, y;
  cairo_get_current_point(cr, &x, &y);

  if (background || width || height) {
    int w, h;
    pango_layout_get_pixel_size(layout, &w, &h);
    if (background) {
      WLB_CAIRO_SET_SOURCE(cr, background);
      cairo_rectangle(cr, x, y, w, h);
      cairo_fill(cr);
      cairo_move_to(cr, x, y);
    }
    if (width) *width = w;
    if (height) *height = h;
  }

  if (foreground) WLB_CAIRO_SET_SOURCE(cr, foreground);
  pango_cairo_show_layout(cr, layout);

  g_object_unref(layout);
  return true;
}

bool wlb_cairo_get_text_size(cairo_t *cr,
                             struct wlb_font *font,
                             const char *text,
                             double scale,
                             bool markup,
                             int *width,
                             int *height) {
  if (!cr || !font || !text) return false;

  PangoLayout *layout = wlb_cairo_get_pango_layout(cr, font, text, scale, markup);
  if (!layout) return false;

  pango_cairo_update_layout(cr, layout);

  int w, h;
  pango_layout_get_pixel_size(layout, &w, &h);
  if (width) *width = w;
  if (height) *height = h;

  g_object_unref(layout);
  return true;
}

bool wlb_cairo_draw_block_base(cairo_t *cr,
                               int w,
                               int h,
                               struct wlb_theme *theme,
                               struct wlb_color_mode *color_mode,
                               bool border) {
  if (!cr || !theme || !color_mode) return false;

  cairo_rectangle(cr, 0, 0, w, h);
  WLB_CAIRO_SET_SOURCE(cr, &color_mode->base);
  cairo_fill(cr);

  if (border) {
    cairo_rectangle(cr, 0, 0, w, h);
    WLB_CAIRO_SET_SOURCE(cr, &color_mode->border);
    cairo_set_line_width(cr, theme->cache.border);
    cairo_stroke(cr);
  }

  return true;
}

bool wlb_cairo_paint(cairo_t *cr, cairo_surface_t *surface, int x, int y) {
  if (!cr || !surface) return false;
  cairo_translate(cr, x, y);
  cairo_set_source_surface(cr, surface, 0, 0);
  cairo_paint(cr);
  cairo_restore(cr);
  return true;
}

bool wlb_cairo_paint_center(cairo_t *cr, int w, int h, cairo_surface_t *surface) {
  if (!cr || !surface) return false;
  int surface_w = cairo_image_surface_get_width(surface);
  int surface_h = cairo_image_surface_get_height(surface);
  wlb_cairo_paint(cr, surface, (w - surface_w) / 2.0, (h - surface_h) / 2.0);
  return true;
}

struct png_reader_closure {
  struct wlb_data data;
  unsigned int position;
};

static cairo_status_t wlb_png_data_reader(void *closure,
                                          unsigned char *data,
                                          unsigned int length) {
  struct png_reader_closure *reader_closure = closure;
  if (reader_closure->position + length > reader_closure->data.size) return CAIRO_STATUS_READ_ERROR;

  memcpy(data, reader_closure->data.ptr + reader_closure->position, length);
  reader_closure->position += length;
  return CAIRO_STATUS_SUCCESS;
}

cairo_surface_t *wlb_cairo_create_surface_from_png_data(struct wlb_data data) {
  if (!WLB_DATA_IS_VALID(&data)) return NULL;

  struct png_reader_closure closure = {
      .data = data,
      .position = 0,
  };
  return cairo_image_surface_create_from_png_stream(wlb_png_data_reader, &closure);
}

cairo_surface_t *wlb_cairo_create_text_buffer(struct wlb_font *font, const char *text, double scale, struct wlb_color *foreground) {
  if (!font || !text || !foreground) return NULL;

  cairo_t *cr = cairo_create(NULL);
  if (!cr) return NULL;
  int w, h;
  bool rv = wlb_cairo_get_text_size(cr, font, text, scale, false, &w, &h);
  cairo_destroy(cr);
  if (!rv) return NULL;

  cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h);
  if (!surface) return NULL;
  if (cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS) goto err_create_cairo;
  cairo_t *cr2 = cairo_create(surface);
  if (!cr2) goto err_create_cairo;
  if (!wlb_cairo_draw_text(cr2, font, text, foreground, NULL, scale, false, NULL, NULL)) goto err_draw_text;
  cairo_destroy(cr2);

  return surface;

err_draw_text:
  cairo_destroy(cr2);
err_create_cairo:
  cairo_surface_destroy(surface);
  return NULL;
}

cairo_surface_t *wlb_cairo_create_svg_buffer(struct wlb_data data, int w, int h) {
  if (!WLB_DATA_IS_VALID(&data)) return NULL;

  GError *gerror = NULL;
  RsvgHandle *handle = rsvg_handle_new_from_data(data.ptr, data.size, &gerror);
  if (!handle) {
    g_error_free(gerror);
    return NULL;
  }

  cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h);
  if (!surface) goto err_create_surface;
  if (cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS) goto err_create_cairo;
  cairo_t *cr = cairo_create(surface);
  if (!cr) goto err_create_cairo;

  RsvgRectangle viewport = {
      .x = 0,
      .y = 0,
      .width = w,
      .height = h,
  };
  if (!rsvg_handle_render_document(handle, cr, &viewport, &gerror)) {
    g_error_free(gerror);
    goto err_render_doc;
  }
  g_object_unref(handle);
  cairo_destroy(cr);

  return surface;

err_render_doc:
  cairo_destroy(cr);
err_create_cairo:
  cairo_surface_destroy(surface);
err_create_surface:
  g_object_unref(handle);
  return NULL;
}

cairo_surface_t *wlb_cairo_create_batch_svg_buffer(struct wlb_data *svgs, int nsvgs,
                                                   int size, int *x_dim) {
  if (!svgs || nsvgs <= 0 || size <= 0) return NULL;

  int dim_x = ceil(sqrt(nsvgs));
  int dim_y = dim_x;
  while (dim_x * dim_y < nsvgs) {
    dim_y++;
  }

  cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, dim_x * size, dim_y * size);
  if (!surface) return NULL;
  if (cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS) goto err_create_cairo;
  cairo_t *cr = cairo_create(surface);
  if (!cr) goto err_create_cairo;

  for (int i = 0; i < nsvgs; i++) {
    if (!WLB_DATA_IS_VALID(&svgs[i])) goto err_render_doc;

    int x = (i % dim_x) * size;
    int y = (i / dim_x) * size;

    GError *gerror = NULL;
    RsvgHandle *handle = rsvg_handle_new_from_data(svgs[i].ptr, svgs[i].size, &gerror);
    if (!handle) {
      g_error_free(gerror);
      goto err_render_doc;
    }

    RsvgRectangle viewport = {
        .x = x,
        .y = y,
        .width = size,
        .height = size,
    };

    gerror = NULL;
    bool rv = rsvg_handle_render_document(handle, cr, &viewport, &gerror);
    if (!rv) g_error_free(gerror);
    g_object_unref(handle);
    if (!rv) goto err_render_doc;
  }

  cairo_destroy(cr);

  if (x_dim) *x_dim = dim_x;
  return surface;

err_render_doc:
  cairo_destroy(cr);
err_create_cairo:
  cairo_surface_destroy(surface);
  return NULL;
}
