#include <string.h>
#include <wayland-util.h>
#include <wlblocks/signal.h>
#include <utl/vector.h>
#include <wayland-client-protocol.h>
#include <wlblocks/registry.h>
#include <stdbool.h>
#include <wayland-client-core.h>
#include <wlblocks/manager.h>
#include <wlblocks/log.h>
#include <wlblocks/output.h>
#include "xdg-shell-protocol.h"

// TODO: Set these up
static void output_add(struct wlb_listener *listener, void *data);
static void output_remove(struct wlb_listener *listener, void *data);
//static void seat_add(struct wlb_listener *listener, void *data);
//static void seat_remove(struct wlb_listener *listener, void *data);
static void pong(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial);
//static void formats(void *data, struct wl_shm *wl_shm, uint32_t format);

static const struct xdg_wm_base_listener base_listener = {
    .ping = pong,
};

//static const struct wl_shm_listener shm_listener = {
//    .format = formats,
//};

void output_add(struct wlb_listener *listener, void *data) {
    struct wlb_manager *manager = wl_container_of(listener, manager, listeners.output_add);
    struct wlb_registry_global *global = data;
    struct wlb_output *output = utl_vector_add(&manager->outputs);
    if (!wlb_output_init(output, global->name, global->global)) {
        wlb_log(WLB_LOG_ERROR, "failed to initialize an output");
        utl_vector_remove(&manager->outputs, manager->outputs.length - 1);
    }
}

void output_remove(struct wlb_listener *listener, void *data) {
    struct wlb_manager *manager = wl_container_of(listener, manager, listeners.output_add);
    struct wlb_registry_global *global = data;
    struct wlb_output *output;
    utl_vector_for_each(output, &manager->outputs) {
        if (output->wl_name != global->name) continue;

        wlb_output_destroy(output);
        utl_vector_remove(&manager->outputs, utl_vector_data_index(output, &manager->outputs));
        break;
    }
}

void pong(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
    xdg_wm_base_pong(xdg_wm_base, serial);
}

//void formats(void *data, struct wl_shm *wl_shm, uint32_t format) {
//    struct wlb_manager *manager = data;
//    switch ((enum wl_shm_format)format) {
//        case WL_SHM_FORMAT_ARGB8888:
//            manager->argb = true;
//	    break;
//        case WL_SHM_FORMAT_XRGB8888:
//            manager->xrgb = true;
//	    break;
//        default:
//            return;
//    }
//}

bool wlb_manager_init(struct wlb_manager *manager, struct wl_display *display) {
    if (!manager) return false;

    memset(manager, 0, sizeof(*manager));

    manager->display = display ? display : wl_display_connect(NULL);
    if (!manager->display) return false;

    // TODO: Add seats init
    if (!utl_vector_init(&manager->outputs, sizeof(struct wlb_output))) {
        wlb_log(WLB_LOG_ERROR, "failed to initialize vectors");
        return false;
    }

    manager->wl_registry = wl_display_get_registry(manager->display);
    if (!manager->wl_registry) {
        wlb_log(WLB_LOG_ERROR, "failed to get wl_registry");
        goto display_err;
    }

    if (!wlb_registry_init(&manager->registry)) {
        wlb_log(WLB_LOG_ERROR, "failed to initialize registry");
        goto wl_registry_err;
    }

    manager->listeners.output_add.notify = output_add;
    manager->listeners.output_remove.notify = output_remove;
    //manager->listeners.seat_add.notify = seat_add;
    //manager->listeners.seat_remove.notify = seat_remove;

    wlb_registry_add_interface(&manager->registry, &wl_output_interface, 0, &manager->listeners.output_add, &manager->listeners.output_remove);
    //wlb_registry_add_interface(&manager->registry, &wl_seat_interface, 0, &manager->listeners.seat_add, &manager->listeners.seat_remove);
    wlb_registry_add_interface(&manager->registry, &xdg_wm_base_interface, 0, NULL, NULL);
    wlb_registry_add_interface(&manager->registry, &wl_compositor_interface, 0, NULL, NULL);
    //wlb_registry_add_interface(&manager->registry, &wl_shm_interface, 0, NULL, NULL);

    if (!wlb_registry_register(&manager->registry, manager->display, manager->wl_registry)) {
        wlb_log(WLB_LOG_ERROR, "failed to register globals");
        goto registry_err;
    }

    //if (wl_shm_add_listener(wlb_registry_get_single(&manager->registry, &wl_shm_interface)->global, &shm_listener, manager) == -1 ||
    //        xdg_wm_base_add_listener(wlb_registry_get_single(&manager->registry, &xdg_wm_base_interface)->global, &base_listener, manager) == -1)  {
    //    wlb_manager_destroy(manager);
    //    return false;
    //}

    return true;

registry_err:
    wlb_registry_destroy(&manager->registry);
wl_registry_err:
    wl_registry_destroy(manager->wl_registry);
display_err:
    if (!display) wl_display_disconnect(manager->display);
    return false;
}

bool wlb_manager_destroy(struct wlb_manager *manager) {
    //wl_shm_destroy(wlb_registry_get_single(&manager->registry, &wl_shm_interface)->global);
    wl_compositor_destroy(wlb_registry_get_single(&manager->registry, &wl_compositor_interface)->global);
    xdg_wm_base_destroy(wlb_registry_get_single(&manager->registry, &xdg_wm_base_interface)->global);
    wlb_registry_destroy(&manager->registry);

    wl_registry_destroy(manager->wl_registry);

    utl_vector_destroy(&manager->outputs);
    //utl_vector_destroy(&manager->seats);

    return true;
}

//struct wlb_window *wlb_manager_create_window(struct wlb_manager *manager) {
//    if (!manager) return NULL;
//
//    struct wlb_xdg_toplevel *toplevel = utl_vector_add(&manager->windows);
//    if (!toplevel) return false;
//
//    if (!wlb_xdg_toplevel_init(toplevel, wlb_registry_get_single(&manager->registry, &xdg_wm_base_interface)->global, wlb_registry_get_single(&manager->registry, &wl_compositor_interface)->global)) {
//        utl_vector_remove(&manager->windows, manager->windows.length - 1);
//        return false;
//    }
//
//    return &toplevel->base;
//}
