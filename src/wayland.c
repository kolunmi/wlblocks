#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client-core.h>
#include <wayland-cursor.h>
#include <xkbcommon/xkbcommon-keysyms.h>
#include <xkbcommon/xkbcommon.h>

#include <EGL/egl.h>
#include <wayland-client-protocol.h>
#include <wayland-egl.h>

#include <rlgl.h>
#include <wayland-util.h>
#include <wlblocks/basic.h>
#include <wlblocks/blocks.h>
#include <wlblocks/crdraw.h>
#include <wlblocks/gldraw.h>
#include <wlblocks/log.h>
#include <wlblocks/wlb_data.h>

#include "xdg-shell-protocol.h"

enum mask {
  MASK_SHIFT,
  MASK_CAPS,
  MASK_CTRL,
  MASK_ALT,
  MASK_MOD2,
  MASK_MOD3,
  MASK_LOGO,
  MASK_MOD5,
  MASK_LAST,
};

const char *XKB_MASK_NAMES[MASK_LAST] = {
    XKB_MOD_NAME_SHIFT,
    XKB_MOD_NAME_CAPS,
    XKB_MOD_NAME_CTRL,
    XKB_MOD_NAME_ALT,
    "Mod2",
    "Mod3",
    XKB_MOD_NAME_LOGO,
    "Mod5",
};

struct seat {
  struct wl_list link;
  struct wlb_wayland *state;
  struct wl_seat *wl_seat;
  struct wl_keyboard *wl_keyboard;
  struct wl_surface *keyboard_surface;
  struct wl_pointer *wl_pointer;
  struct wl_surface *pointer_surface;

  uint32_t name;
  uint32_t button;
  uint32_t button_state;
  uint32_t button_serial;
  int32_t x, y;
  int32_t lx, ly;
  int32_t vscroll, hscroll;
  uint32_t mods;

  struct {
    struct xkb_state *state;
    struct xkb_context *context;
    struct xkb_keymap *keymap;
    xkb_mod_mask_t masks[MASK_LAST];
  } xkb;
};

struct wlb_wayland {
  struct wl_display *display;
  struct wl_registry *registry;
  struct wl_compositor *compositor;
  struct xdg_wm_base *xdg_wm_base;
  struct wl_shm *shm;
  struct wl_list seats;
  struct seat *frame_seat;

  struct wl_cursor_image *cursor_image;
  struct wl_surface *cursor_surface;

  struct wl_surface *surface;
  struct xdg_surface *xdg_surface;
  struct xdg_toplevel *xdg_toplevel;
  struct wl_egl_window *egl_window;

  EGLDisplay egl_display;
  EGLConfig egl_config;
  EGLContext egl_context;
  EGLSurface egl_surface;

  int32_t width;
  int32_t height;
  bool running;

  struct wlb_document *doc;

  struct wlb_listener drag;
  struct wlb_listener warp;
  struct wlb_listener minimize;
  struct wlb_listener maximize;
  struct wlb_listener close;
};

// -----------------------------

void keyboard_keymap(void *data,
                     struct wl_keyboard *wl_keyboard,
                     uint32_t format,
                     int32_t fd,
                     uint32_t size) {
  struct seat *seat = data;

  if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) {
    close(fd);
    return;
  }

  char *map_str = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
  if (map_str == MAP_FAILED) {
    close(fd);
    return;
  }

  struct xkb_keymap *keymap = xkb_keymap_new_from_string(seat->xkb.context, map_str, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
  munmap(map_str, size);
  close(fd);

  if (!keymap) {
    wlb_log(WLB_LOG_ERROR, "Failed to compile keymap");
    return;
  }

  struct xkb_state *state;
  if (!(state = xkb_state_new(keymap))) {
    wlb_log(WLB_LOG_ERROR, "Failed to create XKB state");
    xkb_keymap_unref(keymap);
    return;
  }

  xkb_keymap_unref(seat->xkb.keymap);
  xkb_state_unref(seat->xkb.state);
  seat->xkb.keymap = keymap;
  seat->xkb.state = state;

  for (uint32_t i = 0; i < MASK_LAST; ++i) {
    seat->xkb.masks[i] = 1 << xkb_keymap_mod_get_index(seat->xkb.keymap, XKB_MASK_NAMES[i]);
  }
}

void keyboard_enter(void *data,
                    struct wl_keyboard *wl_keyboard,
                    uint32_t serial,
                    struct wl_surface *surface,
                    struct wl_array *keys) {
  struct seat *seat = data;
  struct wlb_wayland *state = seat->state;

  seat->keyboard_surface = surface;
  state->doc->focused = true;
}

void keyboard_leave(void *data,
                    struct wl_keyboard *wl_keyboard,
                    uint32_t serial,
                    struct wl_surface *surface) {
  struct seat *seat = data;
  struct wlb_wayland *state = seat->state;

  seat->keyboard_surface = NULL;
  state->doc->focused = false;
}

void keyboard_key(void *data,
                  struct wl_keyboard *wl_keyboard,
                  uint32_t serial,
                  uint32_t time,
                  uint32_t key,
                  uint32_t state) {

  struct seat *seat = data;
  struct wlb_wayland *app_state = seat->state;

  xkb_keysym_t sym = xkb_state_key_get_one_sym(seat->xkb.state, key + 8);
  wlb_document_send_key(app_state->doc, sym, key, state);
}

void keyboard_modifiers(void *data,
                        struct wl_keyboard *wl_keyboard,
                        uint32_t serial,
                        uint32_t mods_depressed,
                        uint32_t mods_latched,
                        uint32_t mods_locked,
                        uint32_t group) {
  struct seat *seat = data;

  seat->mods = mods_depressed;
}

void keyboard_repeat_info(void *data,
                          struct wl_keyboard *wl_keyboard,
                          int32_t rate,
                          int32_t delay) {
  struct seat *seat = data;
}

static const struct wl_keyboard_listener keyboard_listener = {
    .keymap = keyboard_keymap,
    .enter = keyboard_enter,
    .leave = keyboard_leave,
    .key = keyboard_key,
    .modifiers = keyboard_modifiers,
    .repeat_info = keyboard_repeat_info,
};

// -----------------------------

static void pointer_enter(void *data, struct wl_pointer *pointer,
                          uint32_t serial, struct wl_surface *surface,
                          wl_fixed_t surface_x, wl_fixed_t surface_y) {
  struct seat *seat = data;
  struct wlb_wayland *wayland = seat->state;

  if (!wayland->cursor_image) {
    const char *size_str = getenv("XCURSOR_SIZE");
    int size = size_str ? atoi(size_str) : 0;
    if (size == 0) size = 24;
    struct wl_cursor_theme *cursor_theme = wl_cursor_theme_load(getenv("XCURSOR_THEME"),
                                                                size * wayland->doc->scale,
                                                                wayland->shm);
    wayland->cursor_image = wl_cursor_theme_get_cursor(cursor_theme, "left_ptr")->images[0];
    wayland->cursor_surface = wl_compositor_create_surface(wayland->compositor);
    wl_surface_set_buffer_scale(wayland->cursor_surface, wayland->doc->scale);
    wl_surface_attach(wayland->cursor_surface, wl_cursor_image_get_buffer(wayland->cursor_image), 0, 0);
    wl_surface_commit(wayland->cursor_surface);
  }

  wl_pointer_set_cursor(pointer, serial, wayland->cursor_surface,
                        wayland->cursor_image->hotspot_x,
                        wayland->cursor_image->hotspot_y);

  seat->pointer_surface = surface;
  seat->lx = -1;
  seat->ly = -1;
  seat->x = wl_fixed_to_int(surface_x);
  seat->y = wl_fixed_to_int(surface_y);
}

static void pointer_leave(void *data, struct wl_pointer *pointer,
                          uint32_t serial, struct wl_surface *surface) {
  struct seat *seat = data;

  seat->pointer_surface = NULL;
  seat->lx = seat->x;
  seat->ly = seat->y;
  seat->x = -1;
  seat->y = -1;
}

static void pointer_button(void *data, struct wl_pointer *pointer, uint32_t serial,
                           uint32_t time, uint32_t button, uint32_t state) {
  struct seat *seat = data;

  seat->button = button;
  seat->button_state = state;
  seat->button_serial = serial;
}

static void pointer_motion(void *data, struct wl_pointer *pointer, uint32_t time,
                           wl_fixed_t surface_x, wl_fixed_t surface_y) {
  struct seat *seat = data;

  seat->lx = seat->x;
  seat->ly = seat->y;
  seat->x = wl_fixed_to_int(surface_x);
  seat->y = wl_fixed_to_int(surface_y);
}

static void pointer_frame(void *data, struct wl_pointer *pointer) {
  struct seat *seat = data;
  struct wlb_wayland *state = seat->state;
  state->frame_seat = seat;

  wlb_document_send_pointer(state->doc, seat->x, seat->y,
                            seat->x - seat->lx, seat->y - seat->ly);

  if (seat->vscroll != 0 || seat->hscroll != 0) {
    bool swap = seat->mods & XKB_KEY_Shift_L;
    int v = swap ? seat->hscroll : seat->vscroll;
    int h = swap ? seat->vscroll : seat->hscroll;

    wlb_document_send_scroll(state->doc,
                             v, h,
                             seat->x, seat->y);
    seat->vscroll = 0;
    seat->hscroll = 0;
  }

  if (seat->button) {
    wlb_document_send_button(state->doc,
                             seat->button,
                             seat->button_state,
                             seat->x, seat->y);

    seat->button = 0;
  }
}

static void pointer_axis(void *data, struct wl_pointer *pointer,
                         uint32_t time, uint32_t axis, wl_fixed_t value) {
  struct seat *seat = data;

  int32_t val = wl_fixed_to_int(value);
  if (axis == WL_POINTER_AXIS_VERTICAL_SCROLL) seat->vscroll = val;
  if (axis == WL_POINTER_AXIS_HORIZONTAL_SCROLL) seat->hscroll = val;
}

static void pointer_axis_discrete(void *data, struct wl_pointer *pointer,
                                  uint32_t axis, int32_t discrete) {
}

static void pointer_axis_source(void *data, struct wl_pointer *pointer,
                                uint32_t axis_source) {
}

static void pointer_axis_stop(void *data, struct wl_pointer *pointer,
                              uint32_t time, uint32_t axis) {
}

static void pointer_axis_value120(void *data, struct wl_pointer *pointer,
                                  uint32_t axis, int32_t discrete) {
}

static const struct wl_pointer_listener pointer_listener = {
    .axis = pointer_axis,
    .axis_discrete = pointer_axis_discrete,
    .axis_source = pointer_axis_source,
    .axis_stop = pointer_axis_stop,
    .axis_value120 = pointer_axis_value120,
    .button = pointer_button,
    .enter = pointer_enter,
    .frame = pointer_frame,
    .leave = pointer_leave,
    .motion = pointer_motion,
};

// -----------------------------

static void seat_capabilities(void *data, struct wl_seat *wl_seat,
                              uint32_t capabilities) {
  struct seat *seat = data;

  uint32_t has_pointer = capabilities & WL_SEAT_CAPABILITY_POINTER;
  if (has_pointer && !seat->wl_pointer) {
    seat->wl_pointer = wl_seat_get_pointer(seat->wl_seat);
    wl_pointer_add_listener(seat->wl_pointer, &pointer_listener, seat);
  } else if (!has_pointer && seat->wl_pointer) {
    wl_pointer_destroy(seat->wl_pointer);
    seat->wl_pointer = NULL;
  }

  uint32_t has_keyboard = capabilities & WL_SEAT_CAPABILITY_KEYBOARD;
  if (has_keyboard && !seat->wl_keyboard) {
    seat->wl_keyboard = wl_seat_get_keyboard(seat->wl_seat);
    wl_keyboard_add_listener(seat->wl_keyboard, &keyboard_listener, seat);
  } else if (!has_keyboard && seat->wl_keyboard) {
    wl_keyboard_destroy(seat->wl_keyboard);
    seat->wl_keyboard = NULL;
  }
}

static void seat_name(void *data, struct wl_seat *wl_seat, const char *name) {
}

static const struct wl_seat_listener seat_listener = {
    .capabilities = seat_capabilities,
    .name = seat_name,
};

// -----------------------------

static void global_registry(void *data, struct wl_registry *wl_registry,
                            uint32_t name, const char *interface, uint32_t version) {
  struct wlb_wayland *state = data;

  if (!strcmp(interface, wl_compositor_interface.name)) {
    state->compositor = wl_registry_bind(wl_registry, name, &wl_compositor_interface, version);

  } else if (!strcmp(interface, xdg_wm_base_interface.name)) {
    state->xdg_wm_base = wl_registry_bind(wl_registry, name, &xdg_wm_base_interface, version);

  } else if (!strcmp(interface, wl_shm_interface.name)) {
    state->shm = wl_registry_bind(wl_registry, name, &wl_shm_interface, version);

  } else if (!strcmp(interface, wl_seat_interface.name)) {
    struct seat *seat = calloc(1, sizeof(struct seat));
    if (!seat) exit(EXIT_FAILURE);
    seat->name = name;
    seat->state = state;
    seat->xkb.context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    if (!seat->xkb.context) exit(EXIT_FAILURE);
    seat->wl_seat = wl_registry_bind(wl_registry, name, &wl_seat_interface, version);
    wl_seat_add_listener(seat->wl_seat, &seat_listener, seat);
    wl_list_insert(state->seats.prev, &seat->link);
  }
}

static void global_remove(void *data, struct wl_registry *wl_registry, uint32_t name) {
  struct wlb_wayland *state = data;

  struct seat *seat;
  wl_list_for_each(seat, &state->seats, link) {
    if (name == seat->name) {
      if (seat->wl_keyboard) wl_keyboard_destroy(seat->wl_keyboard);
      if (seat->wl_pointer) wl_pointer_destroy(seat->wl_pointer);
      wl_seat_destroy(seat->wl_seat);
      wl_list_remove(&seat->link);
      free(seat);
      break;
    }
  }
}

static const struct wl_registry_listener registry_listener = {
    global_registry,
    global_remove,
};

// -----------------------------

static void wm_ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
  xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener wm_base_listener = {
    .ping = wm_ping,
};

// -----------------------------

static void surface_configure(void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
  xdg_surface_ack_configure(xdg_surface, serial);
}

static const struct xdg_surface_listener surface_listener = {
    .configure = surface_configure,
};

// -----------------------------

static void toplevel_configure(void *data, struct xdg_toplevel *xdg_toplevel,
                               int32_t width, int32_t height, struct wl_array *states) {
  struct wlb_wayland *state = data;

  if (!width && !height) return;

  if (state->width != width || state->height != height) {
    state->width = width;
    state->height = height;
    wlb_block_apply_rect(&state->doc->root->block,
                         (struct wlb_rectf){0, 0, width, height});

    rlSetFramebufferWidth(width);
    rlSetFramebufferHeight(height);
    wlb_gl_make_viewport(width, height);

    // ?
    wlb_document_send_pointer(state->doc, -1, -1, 0, 0);

    wl_egl_window_resize(state->egl_window, width, height, 0, 0);
    wl_surface_commit(state->surface);
  }

  bool maximized = false;
  uint32_t *toplevel_state;
  wl_array_for_each(toplevel_state, states) {
    if (*toplevel_state == XDG_TOPLEVEL_STATE_MAXIMIZED) maximized = true;
  }
  state->doc->maximized = maximized;
}

static void toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel) {
  struct wlb_wayland *state = data;

  state->running = false;
}

static void toplevel_caps(void *data,
                          struct xdg_toplevel *xdg_toplevel,
                          struct wl_array *capabilities) {
}

static void toplevel_configure_bounds(void *data,
                                      struct xdg_toplevel *xdg_toplevel,
                                      int32_t width,
                                      int32_t height) {
}

static const struct xdg_toplevel_listener toplevel_listener = {
    .configure = toplevel_configure,
    .close = toplevel_close,
    .wm_capabilities = toplevel_caps,
    .configure_bounds = toplevel_configure_bounds,
};

// -----------------------------

static void wayland_connect(struct wlb_wayland *state) {
  state->display = wl_display_connect(NULL);
  if (!state->display) {
    wlb_log(WLB_LOG_ERROR, "Failed to connect to wayland display");
    exit(EXIT_FAILURE);
  }

  state->registry = wl_display_get_registry(state->display);
  wl_registry_add_listener(state->registry, &registry_listener, state);
  wl_display_roundtrip(state->display);
  if (!state->compositor || !state->xdg_wm_base || !state->shm) {
    wlb_log(WLB_LOG_ERROR, "Compositor does not support all needed protocols");
    exit(EXIT_FAILURE);
  }

  xdg_wm_base_add_listener(state->xdg_wm_base, &wm_base_listener, NULL);

  state->surface = wl_compositor_create_surface(state->compositor);
  state->xdg_surface = xdg_wm_base_get_xdg_surface(state->xdg_wm_base,
                                                   state->surface);
  xdg_surface_add_listener(state->xdg_surface, &surface_listener, NULL);
  state->xdg_toplevel = xdg_surface_get_toplevel(state->xdg_surface);
  xdg_toplevel_set_title(state->xdg_toplevel, "wlblocks example");
  xdg_toplevel_add_listener(state->xdg_toplevel, &toplevel_listener, state);
  wl_surface_commit(state->surface);

  wl_display_roundtrip(state->display);
}

static void egl_init(struct wlb_wayland *state) {
  EGLint major;
  EGLint minor;
  EGLint num_configs;
  EGLint attribs[] = {
      EGL_SURFACE_TYPE,
      EGL_WINDOW_BIT,
      EGL_RENDERABLE_TYPE,
      EGL_OPENGL_ES2_BIT,
      EGL_NONE,
  };
  EGLint context_attribs[] = {
      EGL_CONTEXT_CLIENT_VERSION,
      2,
      EGL_NONE,
  };

  state->egl_window = wl_egl_window_create(state->surface, state->width,
                                           state->height);
  if (!state->egl_window) {
    wlb_log(WLB_LOG_ERROR, "Failed to create egl window");
    exit(EXIT_FAILURE);
  }

  state->egl_display = eglGetDisplay((EGLNativeDisplayType)state->display);
  if (state->display == EGL_NO_DISPLAY) {
    wlb_log(WLB_LOG_ERROR, "Failed to get EGL display");
    exit(EXIT_FAILURE);
  }

  if (eglInitialize(state->egl_display, &major, &minor) != EGL_TRUE) {
    wlb_log(WLB_LOG_ERROR, "Failed to initialize EGL");
    exit(EXIT_FAILURE);
  }

  if (eglChooseConfig(state->egl_display, attribs, &state->egl_config, 1,
                      &num_configs) != EGL_TRUE) {
    wlb_log(WLB_LOG_ERROR, "Failed to find matching EGL config");
    exit(EXIT_FAILURE);
  }

  state->egl_context = eglCreateContext(state->egl_display, state->egl_config,
                                        EGL_NO_CONTEXT, context_attribs);
  if (state->egl_context == EGL_NO_CONTEXT) {
    wlb_log(WLB_LOG_ERROR, "Failed to create EGL context");
    exit(EXIT_FAILURE);
  }

  state->egl_surface = eglCreateWindowSurface(state->egl_display,
                                              state->egl_config,
                                              (EGLNativeWindowType)state->egl_window, NULL);
  if (state->egl_surface == EGL_NO_SURFACE) {
    wlb_log(WLB_LOG_ERROR, "Failed to create EGL surface");
    exit(EXIT_FAILURE);
  }

  if (!eglMakeCurrent(state->egl_display, state->egl_surface,
                      state->egl_surface, state->egl_context)) {
    wlb_log(WLB_LOG_ERROR, "Failed to make EGL context current");
    exit(EXIT_FAILURE);
  }
}

static void draw(struct wlb_wayland *state) {
  wlb_document_draw(state->doc);
  rlDrawRenderBatchActive();
  eglSwapBuffers(state->egl_display, state->egl_surface);
}

static void handle_drag(struct wlb_listener *listener, void *data) {
  struct wlb_wayland *state = utl_container_of(listener, state, drag);

  xdg_toplevel_move(state->xdg_toplevel,
                    state->frame_seat->wl_seat,
                    state->frame_seat->button_serial);
}

static void handle_warp(struct wlb_listener *listener, void *data) {
  struct wlb_wayland *state = utl_container_of(listener, state, warp);
  struct wlb_vec2 *warp = data;

  /* unimplemented */
}

static void handle_minimize(struct wlb_listener *listener, void *data) {
  struct wlb_wayland *state = utl_container_of(listener, state, minimize);

  xdg_toplevel_set_minimized(state->xdg_toplevel);
}

static void handle_maximize(struct wlb_listener *listener, void *data) {
  struct wlb_wayland *state = utl_container_of(listener, state, maximize);

  if (state->doc->maximized) {
    xdg_toplevel_unset_maximized(state->xdg_toplevel);
  } else {
    xdg_toplevel_set_maximized(state->xdg_toplevel);
  }
}

static void handle_close(struct wlb_listener *listener, void *data) {
  struct wlb_wayland *state = utl_container_of(listener, state, close);

  state->running = false;
}

struct wlb_wayland *wlb_init_wayland(struct wlb_document *doc,
                                     int width,
                                     int height) {
  if (!doc) return NULL;

  struct wlb_wayland *wayland = calloc(1, sizeof(*wayland));
  if (!wayland) return NULL;

  wl_list_init(&wayland->seats);

  wayland->doc = doc;
  wayland->width = width;
  wayland->height = height;

  wayland->running = true;
  wayland_connect(wayland);
  egl_init(wayland);

  rlglInit(wayland->width, wayland->height);
  rlSetBlendMode(RL_BLEND_ALPHA_PREMULTIPLY);

  if (!wlb_global_init_resources()) exit(EXIT_FAILURE);
  wlb_gl_make_viewport(wayland->width, wayland->height);

  wayland->drag.notify = handle_drag;
  wlb_signal_add(&wayland->doc->events.request_drag, &wayland->drag);
  wayland->warp.notify = handle_warp;
  wlb_signal_add(&wayland->doc->events.request_warp, &wayland->warp);
  wayland->minimize.notify = handle_minimize;
  wlb_signal_add(&wayland->doc->events.request_minimize, &wayland->minimize);
  wayland->maximize.notify = handle_maximize;
  wlb_signal_add(&wayland->doc->events.request_maximize, &wayland->maximize);
  wayland->close.notify = handle_close;
  wlb_signal_add(&wayland->doc->events.request_close, &wayland->close);

  return wayland;
}

struct wl_display *wlb_wayland_get_display(struct wlb_wayland *wayland) {
  if (!wayland) return NULL;
  return wayland->display;
}

bool wlb_wayland_get_running(struct wlb_wayland *wayland) {
  if (!wayland) return false;
  return wayland->running;
}

bool wlb_wayland_draw_now(struct wlb_wayland *wayland) {
  if (!wayland) return false;
  draw(wayland);
  return true;
}

bool wlb_destroy_wayland(struct wlb_wayland *wayland) {
  if (!wayland) return false;

  rlglClose();
  eglDestroySurface(wayland->egl_display, wayland->egl_surface);
  eglDestroyContext(wayland->egl_display, wayland->egl_context);
  wl_egl_window_destroy(wayland->egl_window);
  xdg_toplevel_destroy(wayland->xdg_toplevel);
  xdg_surface_destroy(wayland->xdg_surface);
  wl_surface_destroy(wayland->surface);
  wl_display_disconnect(wayland->display);

  free(wayland);

  return true;
}
