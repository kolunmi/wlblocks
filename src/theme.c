#include <wlblocks/theme.h>

bool wlb_font_init(struct wlb_font *font, const char *string) {
  PangoFontDescription *desc = pango_font_description_from_string(string);
  if (!desc) return false;

  if (!pango_font_description_get_family(desc)) goto err_parse_font;
  if (!pango_font_description_get_size(desc)) goto err_parse_font;

  char *font_string = strdup(string);
  if (!font_string) goto err_parse_font;

  cairo_t *cairo = cairo_create(NULL);
  if (!cairo) goto err_create_cairo;
  PangoContext *pango = pango_cairo_create_context(cairo);
  if (!pango) goto err_create_pango;
  PangoFontMetrics *metrics = pango_context_get_metrics(pango, desc, NULL);
  if (!metrics) goto err_rest;

  font->desc = desc;
  font->string = font_string;
  font->baseline = pango_font_metrics_get_ascent(metrics) / PANGO_SCALE;
  font->height = font->baseline + pango_font_metrics_get_descent(metrics) / PANGO_SCALE;
  font->markup = false;

  pango_font_metrics_unref(metrics);
  g_object_unref(pango);
  cairo_destroy(cairo);
  return true;

err_rest:
  g_object_unref(pango);
err_create_pango:
  cairo_destroy(cairo);
err_create_cairo:
  free(font_string);
err_parse_font:
  pango_font_description_free(desc);
  return false;
}

bool wlb_font_deinit(struct wlb_font *font) {
  if (!font) return false;
  pango_font_description_free(font->desc);
  free(font->string);
  return true;
}

struct wlb_theme *wlb_theme_create(const char *font_string, const struct wlb_color_config *color_config) {
  if (!font_string || !color_config) return NULL;

  struct wlb_theme *theme = calloc(1, sizeof(struct wlb_theme));
  if (!theme) return NULL;

  if (!wlb_font_init(&theme->font, font_string)) {
    free(theme);
    return NULL;
  }
  theme->color_config = *color_config;

  theme->cache.pad = theme->font.height / 2;
  theme->cache.icon_size = theme->font.height;

  return theme;
}

static const struct wlb_color_config default_color_config = {
    .background = {.r = 0.2, .g = 0.2, .b = 0.2, .a = 1.0},
    .normal = {
        .border = {.r = 0.5, .g = 0.5, .b = 0.5, .a = 1.0},
        .base = {.r = 0.25, .g = 0.3, .b = 0.3, .a = 1.0},
        .text = {.r = 0.9, .g = 0.9, .b = 0.9, .a = 1.0},
    },
    .focused = {
        .border = {.r = 0.35, .g = 0.56, .b = 0.56, .a = 1.0},
        .base = {.r = 0.3, .g = 0.31, .b = 0.31, .a = 1.0},
        .text = {.r = 0.95, .g = 0.96, .b = 0.98, .a = 1.0},
    },
    .pressed = {
        .border = {.r = 0.6, .g = 0.6, .b = 0.6, .a = 1.0},
        .base = {.r = 0.5, .g = 0.5, .b = 0.5, .a = 1.0},
        .text = {.r = 1.0, .g = 1.0, .b = 1.0, .a = 1.0},
    },
    .disabled = {
        .border = {.r = 0.1, .g = 0.1, .b = 0.1, .a = 1.0},
        .base = {.r = 0.1, .g = 0.1, .b = 0.1, .a = 1.0},
        .text = {.r = 0.6, .g = 0.6, .b = 0.6, .a = 1.0},
    },
};

struct wlb_theme *wlb_theme_create_default(void) {
  return wlb_theme_create("monospace 18", &default_color_config);
}

bool wlb_theme_destroy(struct wlb_theme *theme) {
  if (!theme) return false;
  wlb_font_deinit(&theme->font);
  free(theme);
  return true;
}
