#include <wayland-client-protocol.h>
#include <wlblocks/input/keyboard.h>

static void keymap(void *data, struct wl_keyboard *wl_keyboard, uint32_t format, int32_t fd, uint32_t size);
static void enter(void *data, struct wl_keyboard *wl_keyboard, uint32_t serial, struct wl_surface *surface, struct wl_array *keys);
static void leave(void *data, struct wl_keyboard *wl_keyboard, uint32_t serial, struct wl_surface *surface);
static void key(void *data, struct wl_keyboard *wl_keyboard, uint32_t serial, uint32_t time, uint32_t key, uint32_t state);
static void modifiers(void *data, struct wl_keyboard *wl_keyboard, uint32_t serial, uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked, uint32_t group);
static void repeat_info(void *data, struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay);

static struct wl_keyboard_listener keyboard_listener = {

};

bool wlb_keyboard_init(struct wlb_keyboard *keyboard, struct wl_keyboard *wl_keyboard) {
    if (!keyboard || !wl_keyboard) return false;

    return true;
}

bool wlb_keyboard_destroy(struct wlb_keyboard *keyboard) {
    if (!keyboard) return false;

    wl_keyboard_release(keyboard->keyboard);

    return true;
}
