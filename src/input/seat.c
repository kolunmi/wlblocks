#include <stdlib.h>
#include <string.h>
#include <wayland-client-protocol.h>
#include <wlblocks/input/seat.h>
#include <wlblocks/input/pointer.h>
#include <wlblocks/input/touch.h>
#include <wlblocks/log.h>

static void seat_capabilities(void *data, struct wl_seat *wl_seat, uint32_t capabilities);
static void seat_name(void *data, struct wl_seat *wl_seat, const char *name);

static const struct wl_seat_listener seat_listener = {
    .name = seat_name,
    .capabilities = seat_capabilities,
};

void seat_name(void *data, struct wl_seat *wl_seat, const char *name) {
    struct wlb_seat *seat = data;
    free(seat->name);
    seat->name = strdup(name);
}

void seat_capabilities(void *data, struct wl_seat *wl_seat, uint32_t capabilities) {
    struct wlb_seat *seat = data;

    if (!(seat->capabilities & WL_SEAT_CAPABILITY_TOUCH) && (capabilities & WL_SEAT_CAPABILITY_TOUCH)) {
        if (!wlb_touch_init(&seat->touch, wl_seat_get_touch(seat->seat))) {
            wlb_log(WLB_LOG_ERROR, "Failed to initialize wlb_touh");
        }
        else {
            wlb_log(WLB_LOG_DEBUG, "created touch device");
            seat->capabilities ^= WL_SEAT_CAPABILITY_TOUCH;
        }
    }
    if (!(seat->capabilities & WL_SEAT_CAPABILITY_POINTER) && (capabilities & WL_SEAT_CAPABILITY_POINTER)) {
        if (!wlb_pointer_init(&seat->pointer, wl_seat_get_pointer(seat->seat))) {
            wlb_log(WLB_LOG_ERROR, "Failed to initialize wlb_pointer");
        }
        else {
            wlb_log(WLB_LOG_DEBUG, "created pointer device");
            seat->capabilities ^= WL_SEAT_CAPABILITY_POINTER;
        }
    }
    if (!(seat->capabilities & WL_SEAT_CAPABILITY_KEYBOARD) && (capabilities & WL_SEAT_CAPABILITY_KEYBOARD)) {
        if (!wlb_keyboard_init(&seat->keyboard, wl_seat_get_keyboard(seat->seat))) {
            wlb_log(WLB_LOG_ERROR, "Failed to initialize wlb_keyboard");
        }
        else {
            wlb_log(WLB_LOG_DEBUG, "created keyboard device");
            seat->capabilities ^= WL_SEAT_CAPABILITY_KEYBOARD;
        }
    }

    if ((seat->capabilities & WL_SEAT_CAPABILITY_TOUCH) && !(capabilities & WL_SEAT_CAPABILITY_TOUCH)) {
        wlb_touch_destroy(&seat->touch);
        seat->capabilities ^= WL_SEAT_CAPABILITY_TOUCH;
        wlb_log(WLB_LOG_DEBUG, "destroyed touch device");
    }
    if ((seat->capabilities & WL_SEAT_CAPABILITY_POINTER) && !(capabilities & WL_SEAT_CAPABILITY_POINTER)) {
        wlb_pointer_destroy(&seat->pointer);
        seat->capabilities ^= WL_SEAT_CAPABILITY_POINTER;
        wlb_log(WLB_LOG_DEBUG, "destroyed pointer device");
    }
    if ((seat->capabilities & WL_SEAT_CAPABILITY_KEYBOARD) && !(capabilities & WL_SEAT_CAPABILITY_KEYBOARD)) {
        wlb_keyboard_destroy(&seat->keyboard);
        seat->capabilities ^= WL_SEAT_CAPABILITY_KEYBOARD;
        wlb_log(WLB_LOG_DEBUG, "destroyed keyboard device");
    }
}

bool wlb_seat_init(struct wlb_seat *seat, uint32_t wl_name, struct wl_seat *wl_seat) {
    if (!seat || !wl_seat) return false;

    memset(seat, 0, sizeof(*seat));

    seat->wl_name = wl_name;
    seat->seat = wl_seat;

    if (wl_seat_add_listener(wl_seat, &seat_listener, seat) == -1) {
        wlb_log(WLB_LOG_ERROR, "Failed to add wl_seat listener");
        return false;
    }

    return true;
}

bool wlb_seat_destroy(struct wlb_seat *seat) {
    if (!seat) return false;

    // Kind of a hack but gets us to destroy things if they're there.
    seat_capabilities(seat, seat->seat, 0);
    free(seat->name);
    wl_seat_destroy(seat->seat);

    return true;
}
