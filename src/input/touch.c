#include <assert.h>
#include <wlblocks/input/touch.h>
#include <wlblocks/log.h>

bool wlb_touch_init(struct wlb_touch *touch, struct wl_touch *wl_touch) {
    wlb_log(WLB_LOG_ERROR, "you shouldn't be using wlb_touch_init it's not ready");
    assert(false);
}

bool wlb_touch_destroy(struct wlb_touch *touch) {
    wlb_log(WLB_LOG_ERROR, "you shouldn't be using wlb_touch_destroy it's not ready");
    assert(false);
}
