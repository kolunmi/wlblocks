#include <stdint.h>
#include <wayland-client-protocol.h>
#include <wlblocks/input/pointer.h>
#include <wlblocks/signal.h>
#include <wlblocks/log.h>

static void enter(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface, wl_fixed_t surface_x, wl_fixed_t surface_y);
static void leave(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface);
static void motion(void *data, struct wl_pointer *wl_pointer, uint32_t time, wl_fixed_t surface_x, wl_fixed_t surface_y);
static void button(void *data, struct wl_pointer *wl_pointer, uint32_t serial, uint32_t time, uint32_t button, uint32_t state);
static void axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value);
static void frame(void *data, struct wl_pointer *wl_pointer);
static void axis_source(void *data, struct wl_pointer *wl_pointer, uint32_t axis_source);
static void axis_stop(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis);
static void axis_discrete(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t discrete);
static void axis_value120(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t value120);
static void axis_relative_direction(void *data, struct wl_pointer *wl_pointer, uint32_t axis, uint32_t direction);

static const struct wl_pointer_listener pointer_listener = {
    .enter = enter,
    .leave = leave,
    .motion = motion,
    .button = button,
    .axis = axis,
    .frame = frame,
    .axis_source = axis_source,
    .axis_stop = axis_stop,
    .axis_discrete = axis_discrete,
    .axis_value120 = axis_value120,
    .axis_relative_direction = axis_relative_direction,
};

void enter(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface, wl_fixed_t surface_x, wl_fixed_t surface_y) {
    struct wlb_pointer *pointer = data;

    pointer->surface = surface;
    pointer->x = surface_x;
    pointer->y = surface_y;

    wlb_signal_emit(&pointer->events.enter_leave, &serial);
}

void leave(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface) {
    struct wlb_pointer *pointer = data;

    pointer->surface = NULL;
    pointer->x = pointer->y = 0;

    wlb_signal_emit(&pointer->events.enter_leave, &serial);
}

void motion(void *data, struct wl_pointer *wl_pointer, uint32_t time, wl_fixed_t surface_x, wl_fixed_t surface_y) {
    struct wlb_pointer *pointer = data;

    pointer->x = surface_x;
    pointer->y = surface_y;

    wlb_signal_emit(&pointer->events.motion, &time);
}

void button(void *data, struct wl_pointer *wl_pointer, uint32_t serial, uint32_t time, uint32_t button, uint32_t state) {
    struct wlb_pointer *pointer = data;

    if (state == WL_POINTER_BUTTON_STATE_PRESSED) {
        utl_vector_push(&pointer->buttons, &button);
    }
    else {
        uint32_t *btn;
        utl_vector_for_each_rev(btn, &pointer->buttons) {
            if (*btn != button) continue;
            utl_vector_remove(&pointer->buttons, utl_vector_data_index(btn, &pointer->buttons));
            break;
        }
    }

    wlb_signal_emit(&pointer->events.button, &(struct wlb_pointer_button_event){
            .serial = serial,
            .time = time,
            .button = button,
            .state = state,
            });
}

void axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) {
    struct wlb_pointer *pointer = data;
    pointer->axis[axis].value += value;
}

void frame(void *data, struct wl_pointer *wl_pointer) {
    struct wlb_pointer *pointer = data;
    wlb_signal_emit(&pointer->events.frame, NULL);
    // NOTE: I don't know if this is how this works lol
    pointer->axis_source = -1;
    for (int i = 0; i < 2; i++) {
        pointer->axis[i].relative_direction = -1;
        pointer->axis[i].value120 = 0;
        pointer->axis[i].discrete_steps = 0;
        pointer->axis[i].value = 0;
    }
}

void axis_source(void *data, struct wl_pointer *wl_pointer, uint32_t axis_source) {
    struct wlb_pointer *pointer = data;
    pointer->axis_source = axis_source;
}

void axis_stop(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis) {
    struct wlb_pointer *pointer = data;
    pointer->axis[axis].value = 0;
    pointer->axis[axis].value120 = 0;
    pointer->axis[axis].discrete_steps = 0;
    pointer->axis_source = -1;
    for (int i = 0; i < 2; i++) pointer->axis[i].relative_direction = -1;
}

void axis_discrete(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t discrete) {
    struct wlb_pointer *pointer = data;
    pointer->axis[axis].discrete_steps += discrete;
}

void axis_value120(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t value120) {
    struct wlb_pointer *pointer = data;
    pointer->axis[axis].value120 += value120;
}

void axis_relative_direction(void *data, struct wl_pointer *wl_pointer, uint32_t axis, uint32_t direction) {
    struct wlb_pointer *pointer = data;
    pointer->axis[axis].relative_direction = direction;
}

bool wlb_pointer_init(struct wlb_pointer *pointer, struct wl_pointer *wl_pointer) {
    if (!pointer || !wl_pointer) return false;


    if (!utl_vector_init(&pointer->buttons, sizeof(uint32_t))) return false;
    for (int i = 0; i < 2; i++) pointer->axis[i].relative_direction = -1;
    pointer->axis_source = -1;
    pointer->pointer = wl_pointer;

    if (wl_pointer_add_listener(pointer->pointer, &pointer_listener, pointer) == -1) {
        wlb_log(WLB_LOG_ERROR, "Failed to add wl_pointer listener");
        utl_vector_destroy(&pointer->buttons);
        return false;
    }

    return true;
}

bool wlb_pointer_destroy(struct wlb_pointer *pointer) {
    if (!pointer) return false;

    wl_pointer_release(pointer->pointer);
    utl_vector_destroy(&pointer->buttons);

    return true;
}
