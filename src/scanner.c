#include <libxml/parser.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static FILE *output = NULL;

#define DIE(fmt, ...)                                   \
  do {                                                  \
    fprintf(stderr, "ERROR: " fmt "\n", ##__VA_ARGS__); \
    exit(1);                                            \
  } while (0)

static void indent(int level) {
  int i;
  for (i = 0; i < level; i++) {
    fprintf(output, "\t");
  }
}

#define PRINT(level, fmt, ...)                \
  do {                                        \
    indent(level);                            \
    fprintf(output, fmt "\n", ##__VA_ARGS__); \
  } while (0)

static void print_header(xmlNode *node) {
  xmlNode *c;
  for (c = node; c; c = c->next) {
    if (c->type == XML_ELEMENT_NODE) {
      if (strcmp((char *)c->name, "block") == 0) {
        xmlChar *id = xmlGetProp(c, (xmlChar *)"id");
        if (id) {
          xmlChar *type = xmlGetProp(c, (xmlChar *)"type");
          if (!type) DIE("block has no type");
          PRINT(1, "struct wlb_%s *%s;", type, id);
          xmlFree(id);
          xmlFree(type);
        }
        if (c->children) print_header(c->children);
      }
    }
  }
}

/*
static void get_cache_len(xmlNode *node, const char *parent_id, int *len_out) {
  bool cached = false;
  xmlNode *c;
  for (c = node; c; c = c->next) {
    if (c->type == XML_ELEMENT_NODE) {
      if (strcmp((char *)c->name, "block") == 0) {
        if (c->children) {
          xmlChar *id = xmlGetProp(c, (xmlChar *)"id");
          get_cache_len(c->children, (const char *)id, len_out);
          if (id) xmlFree(id);
        }
      } else if (!parent_id && !cached && strcmp((char *)c->name, "property") == 0) {
        xmlChar *bind_source = xmlGetProp(c, (xmlChar *)"bind-source");
        xmlChar *bind_type = xmlGetProp(c, (xmlChar *)"bind-type");
        xmlChar *bind_property = xmlGetProp(c, (xmlChar *)"bind-property");
        if (bind_source && bind_type && bind_property) {
          cached = true;
          (*len_out)++;
        }
        if (bind_source) xmlFree(bind_source);
        if (bind_type) xmlFree(bind_type);
        if (bind_property) xmlFree(bind_property);
      }
    }
  }
}
*/

static void print_code(xmlNode *node, int level, const char *parent, const char *parent_id, int *cache_cnt) {
  /* bool cached = false; */
  int level_name_cnt = 0;
  xmlNode *c;
  for (c = node; c; c = c->next) {
    if (c->type == XML_ELEMENT_NODE) {
      if (strcmp((char *)c->name, "block") == 0) {
        xmlChar *type = xmlGetProp(c, (xmlChar *)"type");
        if (!type) DIE("block has no type");
        char var[32];
        snprintf(var, sizeof(var), "b_%d_%d", level - 1, level_name_cnt);
        if (parent) {
          PRINT(level, "CREATE_BLOCK(%s, %s, &%s->block)", var, type, parent);
        } else {
          PRINT(level, "CREATE_BLOCK(%s, %s, parent)", var, type);
        }

        xmlChar *id = xmlGetProp(c, (xmlChar *)"id");
        if (id) PRINT(level, "ui_out->%s = %s;", id, var);
        xmlFree(type);

        if (c->children) {
          PRINT(level, "do {");
          print_code(c->children, level + 1, var, (const char *)id, cache_cnt);
          PRINT(level, "} while (0);");
        }

        if (id) xmlFree(id);
        level_name_cnt++;
      } else if (strcmp((char *)c->name, "property") == 0) {
        /*
        if (!cached) {
          xmlChar *bind_source = xmlGetProp(c, (xmlChar *)"bind-source");
          xmlChar *bind_type = xmlGetProp(c, (xmlChar *)"bind-type");
          xmlChar *bind_property = xmlGetProp(c, (xmlChar *)"bind-property");
          if (bind_source && bind_type && bind_property && !parent_id) {
            PRINT(level, "cache[%d] = %s;", *cache_cnt, parent);
            cached = true;
            (*cache_cnt)++;
          }
          if (bind_source) xmlFree(bind_source);
          if (bind_type) xmlFree(bind_type);
          if (bind_property) xmlFree(bind_property);
        }
        */

        xmlChar *type = xmlGetProp(c, (xmlChar *)"type");
        if (!type) DIE("property has no type");
        xmlChar *name = xmlGetProp(c, (xmlChar *)"name");
        if (!name) DIE("property has no name");
        xmlChar *value = xmlNodeGetContent(c);
        if (strcmp((char *)type, "block") == 0) {
          PRINT(level, "wlb_%s_set_%s(&%s->block, %s);", type, name, parent, value);
        } else {
          PRINT(level, "wlb_%s_set_%s(%s, %s);", type, name, parent, value);
        }
        xmlFree(type);
        xmlFree(name);
        xmlFree(value);
      }
    }
  }
}

/*
static void print_connections(xmlNode *node, const char *parent_id, int *cache_cnt) {
  bool cached = false;
  xmlNode *c;
  for (c = node; c; c = c->next) {
    if (c->type == XML_ELEMENT_NODE) {
      if (strcmp((char *)c->name, "block") == 0) {
        if (c->children) {
          xmlChar *id = xmlGetProp(c, (xmlChar *)"id");
          print_connections(c->children, (const char *)id, cache_cnt);
          if (id) xmlFree(id);
        }
      } else if (strcmp((char *)c->name, "property") == 0) {
        xmlChar *bind_source = xmlGetProp(c, (xmlChar *)"bind-source");
        xmlChar *bind_type = xmlGetProp(c, (xmlChar *)"bind-type");
        xmlChar *bind_property = xmlGetProp(c, (xmlChar *)"bind-property");
        if (bind_source && bind_type && bind_property) {
          xmlChar *type = xmlGetProp(c, (xmlChar *)"type");
          if (!type) DIE("property has no type");
          xmlChar *name = xmlGetProp(c, (xmlChar *)"name");
          if (!name) DIE("property has no name");
          if (parent_id) {
            PRINT(1, "wlb_block_bind_property((struct wlb_block *)ui_out->%s, struct wlb_%s, %s, (struct wlb_block *)ui_out->%s, struct wlb_%s, %s);", bind_source, bind_type, bind_property, parent_id, type, name);
          } else {
            PRINT(1, "wlb_block_bind_property((struct wlb_block *)ui_out->%s, struct wlb_%s, %s, cache[%d], struct wlb_%s, %s);", bind_source, bind_type, bind_property, *cache_cnt, type, name);
            if (!cached) {
              cached = true;
              (*cache_cnt)++;
            }
          }
          xmlFree(type);
          xmlFree(name);
        }
        if (bind_source) xmlFree(bind_source);
        if (bind_type) xmlFree(bind_type);
        if (bind_property) xmlFree(bind_property);
      }
    }
  }
}
*/

int main(int argc, char **argv) {
  if (argc != 5) DIE("incorrect number of args: \n\twlb-scanner [--header|--code] name input-file output-file");

  bool header;
  if (strcmp(argv[1], "--header") == 0) {
    header = true;
  } else if (strcmp(argv[1], "--code") == 0) {
    header = false;
  } else {
    DIE("please supply either `--header` or `--code` as an option");
  }

  output = fopen(argv[4], "w");
  if (!output) DIE("could not open output file");

  xmlDoc *doc = xmlReadFile(argv[3], NULL, 0);
  if (!doc) DIE("could not parse file");

  PRINT(0, "/* AUTO-GENERATED BY WLB-SCANNER */\n");
  if (header) {
    PRINT(0, "#include <wlblocks/blocks.h>");
    PRINT(0, "#include <wlblocks/wlb_data.h>\n");
    PRINT(0, "/** @struct %s_ui", argv[2]);
    PRINT(0, " *");
    PRINT(0, " * @brief Contains identified blocks for '%s'.", argv[2]);
    PRINT(0, " *");
    PRINT(0, " * A structure providing a simple way to retrieve");
    PRINT(0, " * the identified blocks from the generated tree.");
    PRINT(0, " */");
    PRINT(0, "struct %s_ui {", argv[2]);
    print_header(xmlDocGetRootElement(doc));
    PRINT(0, "};\n");
    PRINT(0, "/** Generate the '%s' custom block tree.", argv[2]);
    PRINT(0, " *");
    PRINT(0, " * @param parent the parent of the root block.");
    PRINT(0, " * @param ui_out the structure into which identified blocks will be written.");
    PRINT(0, " *");
    PRINT(0, " * @return the root block, or NULL on error.");
    PRINT(0, " */");
    PRINT(0, "struct wlb_block *make_%s_ui(struct wlb_block *parent, struct %s_ui *ui_out);", argv[2], argv[2]);
  } else {
    PRINT(0, "#include \"%s-ui.h\"\n", argv[2]);
    PRINT(0, "#define CREATE_BLOCK(var, type, parent) \\");
    PRINT(1, "struct wlb_##type *var = wlb_##type##_create(); \\");
    PRINT(1, "if (!var) { wlb_block_destroy(&b_0_0->block); return NULL; } \\");
    PRINT(1, "wlb_block_add_child(parent, &var->block);\n");
    PRINT(0, "struct wlb_block *make_%s_ui(struct wlb_block *parent, struct %s_ui *ui_out) {", argv[2], argv[2]);
    int cache_cnt = 0;
    /* get_cache_len(xmlDocGetRootElement(doc), NULL, &cache_cnt); */
    if (cache_cnt > 0) PRINT(1, "struct wlb_block *cache[%d];", cache_cnt);
    cache_cnt = 0;
    print_code(xmlDocGetRootElement(doc), 1, NULL, NULL, &cache_cnt);
    cache_cnt = 0;
    /* print_connections(xmlDocGetRootElement(doc), NULL, &cache_cnt); */
    PRINT(1, "return &b_0_0->block;");
    PRINT(0, "}");
  }
  PRINT(0, "\n/* END OF AUTO-GENERATION */");

  xmlFreeDoc(doc);
  xmlCleanupParser();

  fclose(output);

  return 0;
}
