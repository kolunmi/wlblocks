#include <linux/input-event-codes.h>
#include <rlgl.h>
#include <stdint.h>
#include <time.h>
#include <wayland-client-protocol.h>
#include <wlblocks/blocks.h>
#include <wlblocks/log.h>
#include <xkbcommon/xkbcommon.h>

static void tree_block(struct wlb_block *block, struct wlb_document *doc) {
  block->doc = doc;

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    tree_block(c, doc);
  }

  if (block->impl->tree) block->impl->tree(block);
}

static void draw_block(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx) {
  if (!wlb_draw_ctx_push_scissor(draw_ctx, block->rect)) {
    wlb_log(WLB_LOG_ERROR, "failed to push scissor, aborting block draw");
    return;
  }

  if (block->props.debug) wlb_gl_draw_rect(block->rect, &(struct wlb_color){1, 0, 1, 1});
  if (block->impl->draw) block->impl->draw(block, draw_ctx);

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible) continue;
    draw_block(c, draw_ctx);
  }

  wlb_draw_ctx_pop_scissor(draw_ctx);
}

static void arrange_block(struct wlb_block *block) {
  if (block->impl->arrange) {
    block->impl->arrange(block);
    return;
  }

  /* default impl */
  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    if (!c->props.visible) continue;

    wlb_block_apply_rect(c, block->rect);
  }
}

static struct wlb_block *send_pointer(struct wlb_block *block,
                                      int x, int y, int dx,
                                      int dy, int button,
                                      int state, int vscroll,
                                      int hscroll, bool clip) {
  block->hovered = false;

  if (!block->props.visible) return NULL;

  if (!clip) {
    block->hovered = x >= block->rect.x && x < block->rect.x + block->rect.w &&
                     y >= block->rect.y && y < block->rect.y + block->rect.h;
  }

  struct wlb_block *found = NULL;
  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    struct wlb_block *tmp = send_pointer(c, x, y, dx, dy, button, state,
                                         vscroll, hscroll, !block->hovered);
    if (tmp) found = tmp;
  }

  if (!block->impl->get_pointer_priority) return found;

  if (found && found->impl->get_pointer_priority &&
      ((vscroll == 0 && hscroll == 0) || found->impl->pointer_scroll) &&
      (state < 0 || found->impl->pointer_button)) {
    int found_priority = 0;
    found->impl->get_pointer_priority(found, &found_priority);

    int block_priority = 0;
    block->impl->get_pointer_priority(block, &block_priority);

    if (block_priority >= found_priority) return found;
  }

  if (block->hovered || block->dragging) {
    if (state < 0) {
      if (block->impl->pointer_move) block->impl->pointer_move(block, x, y, dx, dy);
    } else {
      block->dragging = block->hovered && state == WL_POINTER_BUTTON_STATE_PRESSED;
      if (block->impl->pointer_button) block->impl->pointer_button(block, button, state, x, y);
    }
  }

  if (block->hovered) {
    if (vscroll != 0 || hscroll != 0) {
      if (block->impl->pointer_scroll) return block;
    } else {
      return block;
    }
  }

  return NULL;
}

static struct wlb_block *send_key(struct wlb_block *block, xkb_keysym_t sym,
                                  uint32_t key, enum wl_keyboard_key_state state) {
  if (!block->props.visible) return NULL;

  struct wlb_block *c;
  utl_list_for_each(c, &block->children, link) {
    struct wlb_block *tmp = send_key(c, sym, key, state);
    if (tmp) return tmp;
  }

  if (block->impl->key && block->props.focused) {
    block->impl->key(block, sym, key, state);
    return block;
  }

  return NULL;
}

bool wlb_document_init(struct wlb_document *doc) {
  if (!doc) return false;

  if (clock_gettime(CLOCK_MONOTONIC, &doc->last_draw) == -1) return false;

  if (!(doc->root = wlb_root_create())) return false;
  doc->root->block.doc = doc;

  wlb_signal_init(&doc->events.request_drag);
  wlb_signal_init(&doc->events.request_warp);
  wlb_signal_init(&doc->events.request_minimize);
  wlb_signal_init(&doc->events.request_maximize);
  wlb_signal_init(&doc->events.request_close);

  doc->needs_refresh = true;

  return true;
}

bool wlb_document_deinit(struct wlb_document *doc) {
  if (!doc) return false;

  wlb_block_destroy(&doc->root->block);
  wlb_theme_destroy(doc->theme);
  free(doc->title);

  return true;
}

bool wlb_document_flush(struct wlb_document *doc) {
  if (!doc) return false;

  while (doc->needs_refresh) {
    doc->needs_refresh = false;
    arrange_block(&doc->root->block);
  }

  return true;
}

bool wlb_document_draw(struct wlb_document *doc) {
  if (!doc) return false;

  wlb_document_flush(doc);

  struct timespec current = {0};
  clock_gettime(CLOCK_MONOTONIC, &current);
  timespec_sub(&doc->delta, &current, &doc->last_draw);
  doc->last_draw = current;

  struct wlb_color *bg = &doc->theme->color_config.background;
  rlClearColor(bg->r * 255, bg->g * 255, bg->b * 255, bg->a * 255);
  rlClearScreenBuffers();

  struct wlb_block_draw_ctx draw_ctx = {0};
  utl_vector_init(&draw_ctx.scissor_stack, sizeof(struct wlb_rectf));

  draw_block(&doc->root->block, &draw_ctx);

  utl_vector_deinit(&draw_ctx.scissor_stack);
  rlDrawRenderBatchActive();
  rlDisableScissorTest();

  return true;
}

bool wlb_document_send_pointer(struct wlb_document *doc, int x, int y, int dx, int dy) {
  if (!doc) return false;

  send_pointer(&doc->root->block, x, y, dx, dy, -1, -1, 0, 0, false);

  return true;
}

bool wlb_document_send_button(struct wlb_document *doc, int button,
                              int state, int x, int y) {
  if (!doc) return false;

  send_pointer(&doc->root->block, x, y, 0, 0,
               button, state, 0, 0, false);

  return true;
}

bool wlb_document_send_scroll(struct wlb_document *doc,
                              int vertical, int horizontal,
                              int x, int y) {
  if (!doc) return false;
  if (vertical == 0 && horizontal == 0) return true;

  struct wlb_block *block = send_pointer(&doc->root->block, x, y, 0, 0, -1, -1,
                                         vertical, horizontal, false);

  if (block) block->impl->pointer_scroll(block, vertical, horizontal, x, y);

  return true;
}

bool wlb_document_send_key(struct wlb_document *doc, xkb_keysym_t sym,
                           uint32_t key, int state) {
  if (!doc) return false;

  send_key(&doc->root->block, sym, key, state);

  return true;
}

static void scissor(struct wlb_block_draw_ctx *draw_ctx) {
  bool first = true;
  struct wlb_rectf area;

  struct wlb_rectf *iter;
  utl_vector_for_each(iter, &draw_ctx->scissor_stack) {
    if (first) {
      area = *iter;
      first = false;
      continue;
    }

    struct wlb_rectf tmp;
    bool rv = wlb_rectf_intersection(&tmp, &area, iter);
    area = tmp;
    if (!rv) break;
  }

  rlEnableScissorTest();
  if (wlb_rectf_empty(&area)) {
    rlScissor(0, 0, 0, 0);
  } else {
    rlScissor(area.x,
              rlGetFramebufferHeight() - (area.y + area.h),
              area.w, area.h);
  }
}

bool wlb_draw_ctx_push_scissor(struct wlb_block_draw_ctx *draw_ctx,
                               struct wlb_rectf rect) {
  if (!draw_ctx) return false;

  struct wlb_rectf *new_rect = utl_vector_add(&draw_ctx->scissor_stack);
  if (!new_rect) return false;

  *new_rect = rect;

  rlDrawRenderBatchActive();
  scissor(draw_ctx);

  return true;
}

bool wlb_draw_ctx_pop_scissor(struct wlb_block_draw_ctx *draw_ctx) {
  if (!draw_ctx) return false;
  if (utl_vector_length(&draw_ctx->scissor_stack) == 0) return false;

  /* end last scissor */
  rlDrawRenderBatchActive();
  rlDisableScissorTest();

  utl_vector_remove(&draw_ctx->scissor_stack,
                    utl_vector_length(&draw_ctx->scissor_stack) - 1);

  if (utl_vector_length(&draw_ctx->scissor_stack) > 0) {
    /* reinitialze the next scissor */
    scissor(draw_ctx);
  }

  return true;
}

bool wlb_draw_ctx_temp_disable_scissor(struct wlb_block_draw_ctx *draw_ctx) {
  rlDrawRenderBatchActive();
  rlDisableScissorTest();

  return true;
}

bool wlb_draw_ctx_reenable_scissor(struct wlb_block_draw_ctx *draw_ctx) {
  if (utl_vector_length(&draw_ctx->scissor_stack) > 0) {
    scissor(draw_ctx);
  }

  return true;
}

bool wlb_block_init(struct wlb_block *block, const struct wlb_block_impl *impl) {
  if (!block || !impl) return false;

  block->impl = impl;

  block->parent = NULL;
  utl_list_init(&block->children);

  block->props.visible = true;

  wlb_signal_init(&block->events.destroyed);
  wlb_signal_init(&block->events.focused);
  wlb_signal_init(&block->events.unfocused);
  wlb_signal_init(&block->events.shown);
  wlb_signal_init(&block->events.hidden);
  wlb_signal_init(&block->events.resized);

  return true;
}

/* this function currently assumes `block` is the first member of every block type */
bool wlb_block_destroy(struct wlb_block *block) {
  if (!block) return false;

  struct wlb_block *c, *t;
  utl_list_for_each_safe(c, t, &block->children, link) {
    wlb_block_destroy(c);
  }

  if (block->impl->destroy) block->impl->destroy(block);
  free(block);

  return true;
}

bool wlb_block_apply_rect(struct wlb_block *block, struct wlb_rectf rect) {
  if (!block) return false;

  struct wlb_vec2 old_size = {block->rect.w, block->rect.h};

  block->rect = rect;
  if (block->props.min_width > 0 && block->rect.w < block->props.min_width) block->rect.w = block->props.min_width;
  if (block->props.min_height > 0 && block->rect.h < block->props.min_height) block->rect.h = block->props.min_height;
  if (block->props.max_width > 0 && block->rect.w > block->props.max_width) block->rect.w = block->props.max_width;
  if (block->props.max_height > 0 && block->rect.h > block->props.max_height) block->rect.h = block->props.max_height;

  if (block->rect.w != old_size.x || block->rect.h != old_size.y) wlb_signal_emit(&block->events.resized, NULL);

  arrange_block(block);

  return true;
}

bool wlb_block_add_child(struct wlb_block *block, struct wlb_block *child) {
  if (!block || !child) return false;

  struct wlb_block *parent = NULL;
  if (block->impl->get_children_parent) block->impl->get_children_parent(block, &parent);
  if (!parent) parent = block;

  utl_list_insert(parent->children.prev, &child->link);
  child->parent = parent;

  tree_block(child, block->doc);

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}

bool wlb_block_get_request_size(struct wlb_block *block,
                                struct wlb_vec2 hint,
                                struct wlb_vec2 *size) {
  if (!block || !size) return false;

  /* if (block->impl->request_size && (hint.x < 0 || hint.y < 0)) { */
  /*   block->impl->request_size(block, hint, scale, size); */
  /* } */

  *size = (struct wlb_vec2){-1, -1};
  if (block->impl->request_size) block->impl->request_size(block, hint, size);

  if (size->x < block->props.min_width) size->x = block->props.min_width;
  if (block->props.max_width > 0 && size->x > block->props.max_width) size->x = block->props.max_width;
  if (size->y < block->props.min_height) size->y = block->props.min_height;
  if (block->props.max_height > 0 && size->y > block->props.max_height) size->y = block->props.max_height;

  block->size_cache = *size;

  return true;
}

bool wlb_block_set_debug(struct wlb_block *block, bool debug) {
  if (!block) return false;
  if (block->props.debug == debug) return true;

  block->props.debug = debug;

  return true;
}

bool wlb_block_set_visible(struct wlb_block *block, bool visible) {
  if (!block) return false;
  if (block->props.visible == visible) return true;

  block->props.visible = visible;

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}

bool wlb_block_set_expand(struct wlb_block *block, bool expand) {
  if (!block) return false;
  if (block->props.expand == expand) return true;

  block->props.expand = expand;

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}

bool wlb_block_set_min_width(struct wlb_block *block, float min_width) {
  if (!block) return false;
  if (block->props.min_width == min_width) return true;

  block->props.min_width = min_width;

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}

bool wlb_block_set_max_width(struct wlb_block *block, float max_width) {
  if (!block) return false;
  if (block->props.max_width == max_width) return true;

  block->props.max_width = max_width;

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}

bool wlb_block_set_min_height(struct wlb_block *block, float min_height) {
  if (!block) return false;
  if (block->props.min_height == min_height) return true;

  block->props.min_height = min_height;

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}

bool wlb_block_set_max_height(struct wlb_block *block, float max_height) {
  if (!block) return false;
  if (block->props.max_height == max_height) return true;

  block->props.max_height = max_height;

  if (block->doc) block->doc->needs_refresh = true;

  return true;
}
