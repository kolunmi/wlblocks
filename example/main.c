#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <wayland-client-protocol.h>

#include <wlblocks/blocks.h>
#include <wlblocks/log.h>
#include <wlblocks/wayland.h>

#include "custom-ui.h"

int main(int argc, char const *argv[]) {
  wlb_log_init(WLB_LOG_DEBUG, NULL);

  struct wlb_document doc = {0};
  struct custom_ui ui;

  if (!wlb_document_init(&doc)) exit(EXIT_FAILURE);
  if (!(doc.theme = wlb_theme_create_default())) exit(EXIT_FAILURE);
  doc.scale = 1;
  doc.title = strdup("wlblocks example");

  struct wlb_wayland *wayland = wlb_init_wayland(&doc, 1000, 800);
  if (!wayland) exit(EXIT_FAILURE);

  if (!make_custom_ui(&doc.root->block, &ui)) exit(EXIT_FAILURE);

  cairo_surface_t *image_surface = cairo_image_surface_create_from_png("data/logo.png");
  struct wlb_texture image_texture;
  bool rv = wlb_gl_texture_from_cairo_surface(&image_texture, image_surface);
  cairo_surface_destroy(image_surface);
  if (!rv) {
    wlb_log(WLB_LOG_ERROR, "Couldn't load 'data/logo.png'");
    exit(EXIT_FAILURE);
  }
  wlb_image_load_texture(ui.image, &image_texture);

  wlb_block_apply_rect(&doc.root->block,
                       (struct wlb_rectf){0, 0, 1000, 800});
  wlb_wayland_draw_now(wayland);

  while (wlb_wayland_get_running(wayland)) {
    struct pollfd fds[1] = {0};
    fds[0].fd = wl_display_get_fd(wlb_wayland_get_display(wayland));
    fds[0].events = POLLIN;

    if (poll(fds, sizeof(fds) / sizeof(fds[0]), -1) > 0) {
      if (fds[0].revents & POLLIN) {
        wl_display_dispatch(wlb_wayland_get_display(wayland));
        wlb_wayland_draw_now(wayland);
      }
    }
  }

  wlb_document_deinit(&doc);
  wlb_destroy_wayland(wayland);

  return EXIT_SUCCESS;
}
