#ifndef WLB_POINTER_H_
#define WLB_POINTER_H_

#include <stdint.h>
#include <stdbool.h>
#include <wayland-client.h>
#include <wayland-util.h>
#include <wlblocks/signal.h>

struct wlb_pointer_button_event {
    uint32_t serial, time, button, state;
};

struct wlb_pointer {
    struct wl_pointer *pointer;

    wl_fixed_t x, y;
    struct utl_vector buttons; // uint32_t
    struct {
        wl_fixed_t value;
        uint32_t discrete_steps;
        int32_t relative_direction,
                value120; // enum wl_pointer_axis_relative_direction || -1 if invalid
        // TODO: Time?
    } axis[2];
    int32_t axis_source; // enum wl_pointer_axis_source || -1 if invalid
    struct wl_surface *surface;

    struct {
        struct wlb_signal enter_leave;
        struct wlb_signal motion;
        struct wlb_signal button;
        struct wlb_signal frame;
        // TODO: Axis?
    } events;
};

bool wlb_pointer_init(struct wlb_pointer *pointer, struct wl_pointer *wl_pointer);
bool wlb_pointer_destroy(struct wlb_pointer *pointer);

#endif // WLB_POINTER_H_
