#ifndef WLB_KEYBOARD_H_
#define WLB_KEYBOARD_H_

#include <stdint.h>
#include <stdbool.h>

struct wlb_keyboard {
    struct wl_keyboard *keyboard;

    struct {
        // TODO:
    } events;
};

bool wlb_keyboard_init(struct wlb_keyboard *keyboard, struct wl_keyboard *wl_keyboard);
bool wlb_keyboard_destroy(struct wlb_keyboard *keyboard);

#endif // WLB_KEYBOARD_H_
