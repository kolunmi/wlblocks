#ifndef WLB_TOUCH_H_
#define WLB_TOUCH_H_

#include <stdint.h>
#include <stdbool.h>
#include <wayland-client.h>

struct wlb_touch {
    struct wl_touch *touch;
};

bool wlb_touch_init(struct wlb_touch *touch, struct wl_touch *wl_touch);
bool wlb_touch_destroy(struct wlb_touch *touch);

#endif // WLB_TOUCH_H_
