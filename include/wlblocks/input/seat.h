#ifndef WLB_SEAT_H_
#define WLB_SEAT_H_

#include <stdint.h>
#include <stdbool.h>
#include <wlblocks/input/touch.h>
#include <wlblocks/input/pointer.h>
#include <wlblocks/input/keyboard.h>

struct wlb_seat {
    uint32_t wl_name;
    struct wl_seat *seat;

    char *name;
    uint32_t capabilities; // enum wl_seat_capability
    struct wlb_touch touch;
    struct wlb_pointer pointer;
    struct wlb_keyboard keyboard;

    struct {
        // TODO: ?
    } events;
};

bool wlb_seat_init(struct wlb_seat *seat, uint32_t wl_name, struct wl_seat *wl_seat);
bool wlb_seat_destroy(struct wlb_seat *seat);

#endif // WLB_SEAT_H_
