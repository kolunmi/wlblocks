#ifndef WLB_LOG_H_
#define WLB_LOG_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

#define wlb_logv(level, fmt, ap) _wlb_logv(level, "[%s:%d]" fmt, __FILE__, __LINE__, ap)
#define wlb_log(level, fmt, ...) _wlb_log(level, "[%s:%d]" fmt, __FILE__, __LINE__, ##__VA_ARGS__)
#define wlb_log_errno(level, fmt, ...) wlb_log(level, fmt " errno: %s", ##__VA_ARGS__, strerror(errno))

enum wlb_log_level {
    WLB_LOG_SILENT = 0,
    WLB_LOG_ERROR = 1,
    WLB_LOG_WARNING = 2,
    WLB_LOG_INFO = 3,
    WLB_LOG_DEBUG = 4,
};

typedef void (*wlb_log_func)(enum wlb_log_level level, const char *fmt, va_list ap);

bool wlb_log_init(enum wlb_log_level level, wlb_log_func log_func);
void _wlb_logv(enum wlb_log_level level, const char *fmt, va_list ap);
void _wlb_log(enum wlb_log_level level, const char *fmt, ...);

void timespec_sub(struct timespec *result, struct timespec *left, struct timespec *right);

#endif // WLB_LOG_H_
