#ifndef WLB_CONNECTION_MANAGER_H_
#define WLB_CONNECTION_MANAGER_H_

#include <wayland-client.h>
#include <utl/vector.h>
#include <wlblocks/registry.h>

struct wlb_manager {
    struct wl_display *display;
    struct wl_registry *wl_registry;

    struct wlb_registry registry;
    struct utl_vector outputs, // struct wlb_output
                      seats;   // struct wlb_seat

    struct {
        struct wlb_listener output_add;
        struct wlb_listener output_remove;
        struct wlb_listener seat_add;
        struct wlb_listener seat_remove;
    } listeners;
};

/// @note if `display` is NULL then we make the connection ourselves.
bool wlb_manager_init(struct wlb_manager *manager, struct wl_display *display);

/// @warning this does not disconnect `manager.display` the user must do this themselves.
bool wlb_manager_destroy(struct wlb_manager *manager);

//struct wlb_window *wlb_manager_create_window(struct wlb_manager *manager);

// TODO: Probably going to need some entry points for the blocks api and stuff from wlb-scanner.
// Also I think per window/popup we are going to hold the buffers then allow the blocks api or
// something to get them from the manager.

#endif // WLB_CONNECTION_MANAGER_H_
