#ifndef WLB_OUTPUT_H_
#define WLB_OUTPUT_H_

#include <stdbool.h>
#include <stdint.h>
#include <wayland-client-protocol.h>

struct wlb_output {
    uint32_t wl_name;
    struct wl_output *output;

    char *name;

    enum wl_output_subpixel subpixel;
    enum wl_output_transform transform;
    uint32_t scale, refresh;
};

bool wlb_output_init(struct wlb_output *output, uint32_t name, struct wl_output *wl_output);
bool wlb_output_destroy(struct wlb_output *output);

#endif // WLB_OUTPUT_H_
