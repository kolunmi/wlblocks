#ifndef WLB_BLOCKS_H_
#define WLB_BLOCKS_H_

#include <time.h>
#include <utl/vector.h>
#include <wlblocks/gldraw.h>
#include <wlblocks/signal.h>
#include <wlblocks/theme.h>
#include <xkbcommon/xkbcommon.h>

/** @struct wlb_document
 *
 * @brief Interfaces a block tree with the rest of wlblocks.
 *
 * A master object used for rigging and manipulating a block tree.
 */
struct wlb_document {
  struct wlb_theme *theme;
  struct wlb_root *root;
  double scale;
  char *title;
  bool focused;
  bool maximized;
  bool needs_refresh;
  struct timespec last_draw, delta;

  struct {
    struct wlb_signal request_drag;
    struct wlb_signal request_warp; // wlb_vec2
    struct wlb_signal request_minimize;
    struct wlb_signal request_maximize;
    struct wlb_signal request_close;
  } events;
};

/** Initialize a document.
 *
 * @param doc the document.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_document
 */
bool wlb_document_init(struct wlb_document *doc);

/** Deinitialize a document, destroying all blocks.
 *
 * @param doc the document.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_document
 */
bool wlb_document_deinit(struct wlb_document *doc);

/** Flush a document, recalculating allocations.
 *
 * @param doc the document.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_document
 */
bool wlb_document_flush(struct wlb_document *doc);

/** Draw a document.
 *
 * @param doc the document.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_document
 */
bool wlb_document_draw(struct wlb_document *doc);

/** Send a pointer motion event to a document.
 *
 * @param doc the document.
 * @param x the x coord; pass -1 if off surface.
 * @param y the y coord; pass -1 if off surface.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_document
 */
bool wlb_document_send_pointer(struct wlb_document *doc, int x, int y, int dx, int dy);

/** Send a pointer button event to a document.
 *
 * @param doc the document.
 * @param button the button pressed.
 * @param state the button state.
 * @param x the x coord; pass -1 if off surface.
 * @param y the y coord; pass -1 if off surface.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_document
 */
bool wlb_document_send_button(struct wlb_document *doc, int button,
                              int state, int x, int y);

/** Send a pointer scroll event to a document.
 *
 * @param doc the document.
 * @param vertical the vertical value; pass -1 if none.
 * @param horizontal the horizontal value; pass -1 if none.
 * @param x the x coord; pass -1 if off surface.
 * @param y the y coord; pass -1 if off surface.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_document
 */
bool wlb_document_send_scroll(struct wlb_document *doc,
                              int vertical, int horizontal,
                              int x, int y);

/** Send a key event to a document.
 *
 * @param doc the document.
 * @param sym the keysym.
 * @param the key value.
 * @param state the state reported by wayland.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_document
 */
bool wlb_document_send_key(struct wlb_document *doc, xkb_keysym_t sym,
                           uint32_t key, int state);

/** @struct wlb_block
 *
 * @brief A basic UI building block.
 *
 * The root class for all blocks. This object serves only as a base for block types,
 * and it will not be found outside of this context.
 */
struct wlb_block {
  struct utl_list link;
  struct wlb_document *doc;
  struct wlb_block *parent;
  struct utl_list children;

  const struct wlb_block_impl *impl;

  struct wlb_rectf rect;
  struct wlb_vec2 size_cache;
  bool hovered;
  bool dragging;

  struct {
    bool debug;
    float min_width;
    float min_height;
    float max_width;
    float max_height;
    bool focused;
    bool visible;
    bool disabled;
    bool expand;
  } props;

  struct {
    struct wlb_signal destroyed;
    struct wlb_signal focused;
    struct wlb_signal unfocused;
    struct wlb_signal shown;
    struct wlb_signal hidden;
    struct wlb_signal resized;
  } events;
};

/** @struct wlb_block_draw_ctx
 *
 * @brief Tracks a drawing context.
 *
 * An object passed to a block's drawing implemenation.
 */
struct wlb_block_draw_ctx {
  struct utl_vector scissor_stack;
};

/** Push a scissor region.
 *
 * @param draw_ctx the drawing context.
 * @param rect the rect.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_block_draw_ctx
 */
bool wlb_draw_ctx_push_scissor(struct wlb_block_draw_ctx *draw_ctx,
                               struct wlb_rectf rect);

/** Pop a scissor region.
 *
 * @param draw_ctx the drawing context.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_block_draw_ctx
 */
bool wlb_draw_ctx_pop_scissor(struct wlb_block_draw_ctx *draw_ctx);

/** Temporarily disable the scissor.
 *
 * @param draw_ctx the drawing context.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block_draw_ctx
 */
bool wlb_draw_ctx_temp_disable_scissor(struct wlb_block_draw_ctx *draw_ctx);

/** Reenable the scissor.
 *
 * @param draw_ctx the drawing context.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block_draw_ctx
 */
bool wlb_draw_ctx_reenable_scissor(struct wlb_block_draw_ctx *draw_ctx);

/** @struct wlb_block_impl
 *
 * @brief Implements a block.
 *
 * An object containing functions which together implement a block type.
 */
struct wlb_block_impl {
  void (*tree)(struct wlb_block *block);
  void (*get_children_parent)(struct wlb_block *block, struct wlb_block **parent);
  void (*draw)(struct wlb_block *block, struct wlb_block_draw_ctx *draw_ctx);
  void (*request_size)(struct wlb_block *block, struct wlb_vec2 hint, struct wlb_vec2 *size);
  void (*arrange)(struct wlb_block *block);
  void (*get_pointer_priority)(struct wlb_block *block, int *priority);
  void (*pointer_move)(struct wlb_block *block, int x, int y, int dx, int dy);
  void (*pointer_button)(struct wlb_block *block, int button, int state, int x, int y);
  void (*pointer_scroll)(struct wlb_block *block, int vscroll, int hscroll, int x, int y);
  void (*key)(struct wlb_block *block, xkb_keysym_t sym, uint32_t key, int state);
  void (*destroy)(struct wlb_block *block);
};

/** Initialize a block.
 *
 * @param block the block.
 * @param type the desired type.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_init(struct wlb_block *block, const struct wlb_block_impl *impl);

/** Destroy a block and all of its children.
 *
 * @param block the block.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_destroy(struct wlb_block *block);

/** Set a block's rectangle.
 *
 * @param block the block.
 * @param rect the desired rect.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_apply_rect(struct wlb_block *block, struct wlb_rectf rect);

/** Append a child to a block's children.
 *
 * @param block the block.
 * @param child the new child.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_add_child(struct wlb_block *block, struct wlb_block *child);

/** Get a block's requested size.
 *
 * @param block the block.
 * @param rect the rect to read into.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_get_request_size(struct wlb_block *block,
                                struct wlb_vec2 hint,
                                struct wlb_vec2 *size);

/** Set a block's debug mode.
 *
 * @param block the block.
 * @param debug the desired debug mode; true to enable.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_debug(struct wlb_block *block, bool debug);

/** Set a block's visibility.
 *
 * @param block the block.
 * @param visible the desired visibility.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_visible(struct wlb_block *block, bool visible);

/** Set a block's expand hint.
 *
 * @param block the block.
 * @param expand the desired value.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_expand(struct wlb_block *block, bool expand);

/** Set a block's minimum width.
 *
 * @param block the block.
 * @param min_width the width.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_min_width(struct wlb_block *block, float min_width);

/** Set a block's maximum width.
 *
 * @param block the block.
 * @param max_width the width.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_max_width(struct wlb_block *block, float max_width);

/** Set a block's minimum height.
 *
 * @param block the block.
 * @param min_height the height.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_min_height(struct wlb_block *block, float min_height);

/** Set a block's maximum height.
 *
 * @param block the block.
 * @param max_height the height.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_block
 */
bool wlb_block_set_max_height(struct wlb_block *block, float max_height);

/** @struct wlb_root
 *
 * @brief Represents a window.
 *
 * A window root.
 */
struct wlb_root {
  struct wlb_block block;

  struct {
  } props;

  struct {
  } events;
};

/** Create a root block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_root
 */
struct wlb_root *wlb_root_create(void);

/** @struct wlb_content
 *
 * @brief Allows for children to be offset and scaled.
 *
 * A fundamental block for implementing dynamic displays.
 */
struct wlb_content {
  struct wlb_block block;

  struct {
    struct wlb_vec2 offset;
    float scale;
  } props;

  struct {
  } events;
};

/** Create a content block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_content
 */
struct wlb_content *wlb_content_create(void);

/** Set a content's offset.
 *
 * @param content the content block.
 * @param offset the offset.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_content
 */
bool wlb_content_set_offset(struct wlb_content *content,
                            struct wlb_vec2 offset);

/** Set a content's scale.
 *
 * @param content the content block.
 * @param scale the scale.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_content
 */
bool wlb_content_set_scale(struct wlb_content *content, float scale);

/** @struct wlb_container
 *
 * @brief Arranges children into a stack.
 *
 * A fundamental block for organizing child blocks.
 */
struct wlb_container {
  struct wlb_block block;

  struct {
    bool vertical;
    int margin;
  } props;

  struct {
    struct wlb_signal sorted;
  } events;
};

/** Create a container block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_container
 */
struct wlb_container *wlb_container_create(void);

/** Set a container's vertical mode.
 *
 * @param container the container.
 * @param vertical the desired value.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_container
 */
bool wlb_container_set_vertical(struct wlb_container *container, bool vertical);

/** Set a container's padding.
 *
 * @param container the container.
 * @param margin the margin.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_container
 */
bool wlb_container_set_margin(struct wlb_container *container, int margin);

/** @struct wlb_separator
 *
 * @brief Draws a separator rectangle.
 *
 * A block useful for visually separating block trees.
 */
struct wlb_separator {
  struct wlb_block block;

  struct {
  } props;

  struct {
  } events;
};

/** Create a separator block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_separator
 */
struct wlb_separator *wlb_separator_create(void);

/** @struct wlb_scrollbar
 *
 * @brief Scrolls over a range.
 *
 * A supporting block used by a scroll_window
 */
struct wlb_scrollbar {
  struct wlb_block block;

  bool dragging;
  float drag_offset;

  struct {
    bool horizontal;
    float position;
    float ratio;
  } props;

  struct {
    struct wlb_signal scrolled;
  } events;
};

/** Create a scrollbar block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_scrollbar
 */
struct wlb_scrollbar *wlb_scrollbar_create(void);

/** Set a scrollbar's horizontal mode.
 *
 * @param scrollbar the scrollbar.
 * @param horizontal the desired value.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_scrollbar
 */
bool wlb_scrollbar_set_horizontal(struct wlb_scrollbar *scrollbar, bool horizontal);

/** Set a scrollbar's position.
 *
 * @param scrollbar the scrollbar.
 * @param position the position.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_scrollbar
 */
bool wlb_scrollbar_set_position(struct wlb_scrollbar *scrollbar, float position);

/** Set the ratio of the viewable size to the content size of a scrollbar.
 *
 * @param scrollbar the scrollbar.
 * @param ratio the ratio value.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_scrollbar
 */
bool wlb_scrollbar_set_ratio(struct wlb_scrollbar *scrollbar, float ratio);

/** @struct wlb_scroll_window
 *
 * @brief Allows children to be scrolled.
 *
 * A fundamental block for panning across children. If its children draw beyond its
 * rect, it will provide a vertical and/or horizontal scroll bars for navigation.
 */
struct wlb_scroll_window {
  struct wlb_block block;

  struct {
    struct wlb_vec2 position;
  } props;

  struct {
    struct wlb_signal scrolled;
  } events;

  /* PRIVATE STATE */
  void *private;
  struct wlb_listener vscroll;
  struct wlb_listener hscroll;
  struct wlb_listener content_change;
  struct wlb_listener window_change;
};

/** Create a scroll_window block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_scroll_window
 */
struct wlb_scroll_window *wlb_scroll_window_create(void);

/** Set a scroll_window's position, or percentage offset.
 *
 * @param scroll_window the scroll_window.
 * @param position the position.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_scroll_window
 */
bool wlb_scroll_window_set_position(struct wlb_scroll_window *scroll_window,
                                    struct wlb_vec2 position);

/** @struct wlb_canvas
 *
 * @brief Allows children to be panned and zoomed.
 *
 * A block useful for graphs and images.
 */
struct wlb_canvas {
  struct wlb_block block;

  struct {
  } props;

  struct {
    struct wlb_signal scrolled;
  } events;

  /* PRIVATE STATE */
  void *private;
};

/** Create a canvas block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_canvas
 */
struct wlb_canvas *wlb_canvas_create(void);

/** @struct wlb_label
 *
 * @brief Displays text.
 *
 * A single-line text block.
 */
struct wlb_label {
  struct wlb_block block;

  struct wlb_texture text_tex;

  struct {
    char *text;
  } props;

  struct {
  } events;
};

/** Create a label block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_label
 */
struct wlb_label *wlb_label_create(void);

/** Set a label's text.
 *
 * @param label the label.
 * @param text the new text.
 *
 * @return true on success, false on invalid args or memory error.
 *
 * @memberof wlb_label
 */
bool wlb_label_set_text(struct wlb_label *label, const char *text);

/** @struct wlb_button
 *
 * @brief Displays an interactive button.
 *
 * A basic button with text and icon support.
 */
struct wlb_button {
  struct wlb_block block;

  bool pressed;

  struct {
    int icon;
    bool depth;
  } props;

  struct {
    struct wlb_signal pressed;
  } events;
};

/** Create a button block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_button
 */
struct wlb_button *wlb_button_create(void);

/** Set a button's icon.
 *
 * @param button the button.
 * @param icon the icon index.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_button
 */
bool wlb_button_set_icon(struct wlb_button *button, int icon);

/** Set a button's depth look.
 *
 * @param button the button.
 * @param depth the desired value; true to offset the button when pressed.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_button
 */
bool wlb_button_set_depth(struct wlb_button *button, bool depth);

/** @struct wlb_spinner
 *
 * @brief Displays an animated spinner.
 *
 * A basic spinner to indicate activity.
 */
struct wlb_spinner {
  struct wlb_block block;

  float rotation;

  struct {
    bool clockwise;
  } props;

  struct {
  } events;
};

/** Create a spinner block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_spinner
 */
struct wlb_spinner *wlb_spinner_create(void);

/** Set a spinner's direction.
 *
 * @param spinner the spinner.
 * @param clockwise whether to go clockwise or counter-clockwise.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_spinner
 */
bool wlb_spinner_set_clockwise(struct wlb_spinner *spinner, bool clockwise);

/** @struct wlb_image
 *
 * @brief Displays an image.
 *
 * A basic image to indicate activity.
 */
struct wlb_image {
  struct wlb_block block;

  struct {
    struct wlb_texture texture;
  } props;

  struct {
  } events;
};

/** Create an image block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_image
 */
struct wlb_image *wlb_image_create(void);

/** Set an image's data.
 *
 * @param image the image.
 * @param texture the texture.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_image
 */
bool wlb_image_load_texture(struct wlb_image *image, struct wlb_texture *texture);

/** @struct wlb_header_bar
 *
 * @brief Displays a header bar.
 *
 * An advanced block intended to be used as a functional window decoration.
 */
struct wlb_header_bar {
  struct wlb_block block;

  struct {
    bool show_buttons;
    bool show_title;
  } props;

  struct {
    struct wlb_signal dragged;
  } events;

  /* PRIVATE STATE */
  void *private;
  struct wlb_listener minimize;
  struct wlb_listener maximize;
  struct wlb_listener close;
};

/** Create a header_bar block.
 *
 * @return The object on success, false on invalid args or memory error.
 *
 * @memberof wlb_header_bar
 */
struct wlb_header_bar *wlb_header_bar_create(void);

#endif // WLB_BLOCKS_H_
