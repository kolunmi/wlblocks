#ifndef WLB_WLB_DATA_H_
#define WLB_WLB_DATA_H_

#include <wlblocks/gldraw.h>

extern unsigned char _wlbdata_add[];
extern unsigned int _wlbdata_add_len;

extern unsigned char _wlbdata_arrow_down[];
extern unsigned int _wlbdata_arrow_down_len;

extern unsigned char _wlbdata_arrow_left[];
extern unsigned int _wlbdata_arrow_left_len;

extern unsigned char _wlbdata_arrow_right[];
extern unsigned int _wlbdata_arrow_right_len;

extern unsigned char _wlbdata_arrow_up[];
extern unsigned int _wlbdata_arrow_up_len;

extern unsigned char _wlbdata_bin[];
extern unsigned int _wlbdata_bin_len;

extern unsigned char _wlbdata_burger[];
extern unsigned int _wlbdata_burger_len;

extern unsigned char _wlbdata_button_pressed[];
extern unsigned int _wlbdata_button_pressed_len;

extern unsigned char _wlbdata_button[];
extern unsigned int _wlbdata_button_len;

extern unsigned char _wlbdata_check[];
extern unsigned int _wlbdata_check_len;

extern unsigned char _wlbdata_clipboard[];
extern unsigned int _wlbdata_clipboard_len;

extern unsigned char _wlbdata_close[];
extern unsigned int _wlbdata_close_len;

extern unsigned char _wlbdata_copy[];
extern unsigned int _wlbdata_copy_len;

extern unsigned char _wlbdata_download[];
extern unsigned int _wlbdata_download_len;

extern unsigned char _wlbdata_down[];
extern unsigned int _wlbdata_down_len;

extern unsigned char _wlbdata_file_copy[];
extern unsigned int _wlbdata_file_copy_len;

extern unsigned char _wlbdata_file_cross[];
extern unsigned int _wlbdata_file_cross_len;

extern unsigned char _wlbdata_file[];
extern unsigned int _wlbdata_file_len;

extern unsigned char _wlbdata_folder_cross[];
extern unsigned int _wlbdata_folder_cross_len;

extern unsigned char _wlbdata_folder[];
extern unsigned int _wlbdata_folder_len;

extern unsigned char _wlbdata_fold[];
extern unsigned int _wlbdata_fold_len;

extern unsigned char _wlbdata_gear[];
extern unsigned int _wlbdata_gear_len;

extern unsigned char _wlbdata_grid[];
extern unsigned int _wlbdata_grid_len;

extern unsigned char _wlbdata_left[];
extern unsigned int _wlbdata_left_len;

extern unsigned char _wlbdata_magnifier[];
extern unsigned int _wlbdata_magnifier_len;

extern unsigned char _wlbdata_maximize[];
extern unsigned int _wlbdata_maximize_len;

extern unsigned char _wlbdata_minimize[];
extern unsigned int _wlbdata_minimize_len;

extern unsigned char _wlbdata_new_file[];
extern unsigned int _wlbdata_new_file_len;

extern unsigned char _wlbdata_new_folder[];
extern unsigned int _wlbdata_new_folder_len;

extern unsigned char _wlbdata_off[];
extern unsigned int _wlbdata_off_len;

extern unsigned char _wlbdata_on[];
extern unsigned int _wlbdata_on_len;

extern unsigned char _wlbdata_options[];
extern unsigned int _wlbdata_options_len;

extern unsigned char _wlbdata_paste[];
extern unsigned int _wlbdata_paste_len;

extern unsigned char _wlbdata_pause[];
extern unsigned int _wlbdata_pause_len;

extern unsigned char _wlbdata_pen[];
extern unsigned int _wlbdata_pen_len;

extern unsigned char _wlbdata_play[];
extern unsigned int _wlbdata_play_len;

extern unsigned char _wlbdata_redo[];
extern unsigned int _wlbdata_redo_len;

extern unsigned char _wlbdata_restore[];
extern unsigned int _wlbdata_restore_len;

extern unsigned char _wlbdata_right[];
extern unsigned int _wlbdata_right_len;

extern unsigned char _wlbdata_scissor[];
extern unsigned int _wlbdata_scissor_len;

extern unsigned char _wlbdata_search[];
extern unsigned int _wlbdata_search_len;

extern unsigned char _wlbdata_settings[];
extern unsigned int _wlbdata_settings_len;

extern unsigned char _wlbdata_stop[];
extern unsigned int _wlbdata_stop_len;

extern unsigned char _wlbdata_undo[];
extern unsigned int _wlbdata_undo_len;

extern unsigned char _wlbdata_upload[];
extern unsigned int _wlbdata_upload_len;

extern unsigned char _wlbdata_up[];
extern unsigned int _wlbdata_up_len;

extern unsigned char _wlbdata_windows[];
extern unsigned int _wlbdata_windows_len;

extern unsigned char _wlbdata_zip[];
extern unsigned int _wlbdata_zip_len;

enum wlbdata_icon {
  WLB_ICON_ADD = 0,
  WLB_ICON_ARROW_DOWN,
  WLB_ICON_ARROW_LEFT,
  WLB_ICON_ARROW_RIGHT,
  WLB_ICON_ARROW_UP,
  WLB_ICON_BIN,
  WLB_ICON_BURGER,
  WLB_ICON_BUTTON_PRESSED,
  WLB_ICON_BUTTON,
  WLB_ICON_CHECK,
  WLB_ICON_CLIPBOARD,
  WLB_ICON_CLOSE,
  WLB_ICON_COPY,
  WLB_ICON_DOWNLOAD,
  WLB_ICON_DOWN,
  WLB_ICON_FILE_COPY,
  WLB_ICON_FILE_CROSS,
  WLB_ICON_FILE,
  WLB_ICON_FOLDER_CROSS,
  WLB_ICON_FOLDER,
  WLB_ICON_FOLD,
  WLB_ICON_GEAR,
  WLB_ICON_GRID,
  WLB_ICON_LEFT,
  WLB_ICON_MAGNIFIER,
  WLB_ICON_MAXIMIZE,
  WLB_ICON_MINIMIZE,
  WLB_ICON_NEW_FILE,
  WLB_ICON_NEW_FOLDER,
  WLB_ICON_OFF,
  WLB_ICON_ON,
  WLB_ICON_OPTIONS,
  WLB_ICON_PASTE,
  WLB_ICON_PAUSE,
  WLB_ICON_PEN,
  WLB_ICON_PLAY,
  WLB_ICON_REDO,
  WLB_ICON_RESTORE,
  WLB_ICON_RIGHT,
  WLB_ICON_SCISSOR,
  WLB_ICON_SEARCH,
  WLB_ICON_SETTINGS,
  WLB_ICON_STOP,
  WLB_ICON_UNDO,
  WLB_ICON_UPLOAD,
  WLB_ICON_UP,
  WLB_ICON_WINDOWS,
  WLB_ICON_ZIP,
  _WLB_ICON_MAX,
};

/** Initialize global resources like icon textures.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_global_init_resources(void);

/** Get the icon texture atlas and rect for a given icon.
 *
 * @param dest the dest texture.
 * @param rect the dest rect.
 * @param icon the icon index.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_global_get_icon(struct wlb_texture *dest,
			 struct wlb_rectf *rect,
			 enum wlbdata_icon icon);

#endif // WLB_WLB_DATA_H_
