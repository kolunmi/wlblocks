#ifndef WLB_WAYLAND_H_
#define WLB_WAYLAND_H_

#include <wlblocks/blocks.h>

/** @struct wlb_wayland
 *
 * @brief Handles wayland functionality.
 *
 * An opaque object to provide basic wayland functionality.
 */
struct wayland;

///

struct wlb_wayland *wlb_init_wayland(struct wlb_document *doc,
                                     int width,
                                     int height);

struct wl_display *wlb_wayland_get_display(struct wlb_wayland *wayland);

bool wlb_wayland_get_running(struct wlb_wayland *wayland);

bool wlb_wayland_draw_now(struct wlb_wayland *wayland);

bool wlb_destroy_wayland(struct wlb_wayland *wayland);

#endif // WLB_WAYLAND_H_
