#ifndef WLB_CRDRAW_H_
#define WLB_CRDRAW_H_

#include <wlblocks/theme.h>

/** Create a pango layout.
 *
 * @param cr the cairo context.
 * @param font the font.
 * @param text the text.
 * @param scale the scale of the text.
 * @param markup parse markup.
 *
 * @return the layout on success, NULL on invalid args or memory error.
 */
PangoLayout *wlb_cairo_get_pango_layout(cairo_t *cr,
                                        struct wlb_font *font,
                                        const char *text,
                                        double scale,
                                        bool markup);

/** Draw text on a surface.
 *
 * @param cr the cairo context.
 * @param font the font.
 * @param text the text.
 * @param foreground the foreground color.
 * @param background the background color.
 * @param scale the scale of the text.
 * @param markup parse markup.
 * @param width width out.
 * @param height height out.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_cairo_draw_text(cairo_t *cr,
                         struct wlb_font *font,
                         const char *text,
                         struct wlb_color *foreground,
                         struct wlb_color *background,
                         double scale,
                         bool markup,
                         int *width,
                         int *height);

/** Get the dimensions of the text that would be drawn with wlb_cairo_draw_text().
 *
 * @param cr the cairo context.
 * @param font the font.
 * @param text the text.
 * @param scale the scale of the text.
 * @param markup parse markup.
 * @param width width out.
 * @param height height out.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_cairo_get_text_size(cairo_t *cr,
                             struct wlb_font *font,
                             const char *text,
                             double scale,
                             bool markup,
                             int *width,
                             int *height);

/**
 * Set a cairo context source color with a struct wlb_color.
 */
#define WLB_CAIRO_SET_SOURCE(cr, color) \
  cairo_set_source_rgba(cr, (color)->b, (color)->g, (color)->r, (color)->a)

/** Draw an empty frame.
 *
 * @param cr the cairo context.
 * @param w the width of the context.
 * @param h the height of the context.
 * @param theme a theme.
 * @param color_mode the desired color mode.
 * @param border draw a border.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_cairo_draw_block_base(cairo_t *cr,
                               int w,
                               int h,
                               struct wlb_theme *theme,
                               struct wlb_color_mode *color_mode,
                               bool border);

/** Paint a surface onto a cairo context.
 *
 * @param cr the cairo context.
 * @param surface the surface.
 * @param x the target location's x coordinate.
 * @param y the target location's y coordinate.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_cairo_paint(cairo_t *cr, cairo_surface_t *surface, int x, int y);

/** Paint a surface onto a cairo context in a centered position.
 *
 * @param cr the cairo context.
 * @param w the width of the context.
 * @param h the height of the context.
 * @param surface the surface.
 *
 * @return true on success, false on invalid args or memory error.
 */
bool wlb_cairo_paint_center(cairo_t *cr, int w, int h, cairo_surface_t *surface);

/** Create a surface from png data.
 *
 * @param data the data.
 *
 * @return the surface on success, NULL on invalid args or memory error.
 */
cairo_surface_t *wlb_cairo_create_surface_from_png_data(struct wlb_data data);

/** Create a surface with text.
 *
 * @param font the font.
 * @param text the text.
 * @param scale the scale of the text.
 * @param foreground the text color.
 *
 * @return the surface on success, NULL on invalid args or memory error.
 */
cairo_surface_t *wlb_cairo_create_text_buffer(struct wlb_font *font,
                                              const char *text,
                                              double scale,
                                              struct wlb_color *foreground);

/** Create a surface with an svg.
 *
 * @param data the svg data.
 * @param w the desired width.
 * @param h the desired height.
 *
 * @return the surface on success, NULL on invalid args or memory error.
 */
cairo_surface_t *wlb_cairo_create_svg_buffer(struct wlb_data data, int w, int h);

/** Create a surface with an svg.
 *
 * @param svgs the svg datas.
 * @param nsvgs the number of svgs.
 * @param size the size of each icon.
 * @param x_dim the calculated amount of icons in a row out.
 *
 * @return the surface on success, NULL on invalid args or memory error.
 */
cairo_surface_t *wlb_cairo_create_batch_svg_buffer(struct wlb_data *svgs, int nsvgs,
						   int size, int *x_dim);

#endif // WLB_CRDRAW_H_
