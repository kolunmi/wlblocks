#ifndef WLB_THEME_H_
#define WLB_THEME_H_

#include <pango/pangocairo.h>
#include <stdbool.h>
#include <wlblocks/basic.h>

/** @struct wlb_color_mode
 *
 * @brief A basic color scheme for a color_config.
 *
 * Defines a single palette for a block.
 */
struct wlb_color_mode {
  struct wlb_color border;
  struct wlb_color base;
  struct wlb_color text;
};

/** @struct wlb_color_config
 *
 * @brief A complete color config for a theme.
 *
 * Defines the palette for a theme.
 */
struct wlb_color_config {
  struct wlb_color background;
  struct wlb_color_mode normal;
  struct wlb_color_mode focused;
  struct wlb_color_mode pressed;
  struct wlb_color_mode disabled;
};

/** @struct wlb_font
 *
 * @brief A font.
 *
 * Describes a text font for a theme.
 */
struct wlb_font {
  PangoFontDescription *desc;
  char *string;
  int height;
  int baseline;
  bool markup;
};

/** Intitialize a font.
 *
 * @param font the font to initialize.
 * @param string the font name.
 *
 * @return true on success, false on invalid args, font string parsing error, or memory error.
 *
 * @memberof wlb_font
 */
bool wlb_font_init(struct wlb_font *font, const char *string);

/** Free a font's contents.
 *
 * @param font the font.
 *
 * @return true on success, false on invalid args.
 *
 * @memberof wlb_font
 */
bool wlb_font_deinit(struct wlb_font *font);

/** @struct wlb_theme
 *
 * @brief An object for configuring the appearance of a block.
 *
 * Contains and caches data relevant to a block's font, colors, offsets, and padding.
 */
struct wlb_theme {
  struct wlb_font font;
  struct wlb_color_config color_config;

  struct {
    int pad;
    int icon_size;
    int border;
  } cache;
};

/** Create a theme.
 *
 * @param font_string the font to initialize.
 * @param color_config the color configuration.
 *
 * @return the newly created theme if successful, NULL on a memory error or font parse error.
 *
 * @memberof wlb_theme
 */
struct wlb_theme *wlb_theme_create(const char *font_string, const struct wlb_color_config *color_config);

/** Create the default theme.
 *
 * @return the newly created theme if successful, NULL on a memory error or font parse error.
 *
 * @memberof wlb_theme
 */
struct wlb_theme *wlb_theme_create_default(void);

/** Free a theme.
 *
 * @param theme the theme.
 *
 * @return true if successful, false on invalid args.
 *
 * @memberof wlb_theme
 */
bool wlb_theme_destroy(struct wlb_theme *theme);

#endif // WLB_THEME_H_
