#ifndef WLB_BASIC_H_
#define WLB_BASIC_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/** @struct wlb_data
 *
 * @brief A location and size of data.
 *
 * Refers to a block of memory.
 */
struct wlb_data {
  uint8_t *ptr;
  size_t size;
};

/**
 * Check if a struct wlb_data can be used.
 */
#define WLB_DATA_IS_VALID(data) ((data)->ptr && (data)->size)

/** @struct wlb_rect
 *
 * @brief An integer rectangle.
 *
 * Used for allocating areas.
 */
struct wlb_rect {
  int x, y, w, h;
};

/** Check if a rect is empty.
 *
 * @param rect the rect.
 *
 * @return true if empty, false on invalid args or not empty.
 *
 * @memberof wlb_rect
 */
bool wlb_rect_empty(struct wlb_rect *rect);

/** Get the intersection of two rects.
 *
 * @param dest the rect to write to.
 *
 * @return true on success, false on invalid args or an empty intersection.
 *
 * @memberof wlb_rectf
 */
bool wlb_rect_intersection(struct wlb_rect *dest,
                           struct wlb_rect *rect_a,
                           struct wlb_rect *rect_b);

/** @struct wlb_color
 *
 * @brief A rgba color.
 *
 * Used for drawing.
 */
struct wlb_color {
  float r, g, b, a;
};

/** @struct wlb_vec2
 *
 * @brief A 2D vector.
 *
 * Used for referencing positions.
 */
struct wlb_vec2 {
  float x, y;
};

/** @struct wlb_rectf
 *
 * @brief A float rectangle.
 *
 * Used for allocating areas.
 */
struct wlb_rectf {
  float x, y, w, h;
};

/** Check if a rect is empty.
 *
 * @param rect the rect.
 *
 * @return true if empty, false on invalid args or not empty.
 *
 * @memberof wlb_rectf
 */
bool wlb_rectf_empty(struct wlb_rectf *rect);

/** Find the closest point to a rect.
 *
 * @param rect the rect.
 * @param x the x coord.
 * @param y the y coord.
 * @param dest_x the resulting x coord.
 * @param dest_y the resulting y coord.
 *
 * @return true on sucess, false on invalid args or rect is empty.
 *
 * @memberof wlb_rectf
 */
bool wlb_rectf_closest_point(struct wlb_rectf *rect, float x, float y,
                             float *dest_x, float *dest_y);

/** Get the intersection of two rects.
 *
 * @param dest the rect to write to.
 *
 * @return true on success, false on invalid args or an empty intersection.
 *
 * @memberof wlb_rectf
 */
bool wlb_rectf_intersection(struct wlb_rectf *dest,
                            struct wlb_rectf *rect_a,
                            struct wlb_rectf *rect_b);

#endif // WLB_BASIC_H_
