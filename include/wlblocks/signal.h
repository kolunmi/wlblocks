#ifndef WLB_SIGNAL_H_
#define WLB_SIGNAL_H_

#include <utl/vector.h>
#include <utl/list.h>

struct wlb_signal;
struct wlb_listener;

typedef void (*wlb_listener_func)(struct wlb_listener *listener, void *data);

struct wlb_signal {
    struct utl_list listeners;
};

struct wlb_listener {
    struct utl_list link;
    wlb_listener_func notify;
};

bool wlb_signal_init(struct wlb_signal *signal);
bool wlb_signal_add(struct wlb_signal *signal, struct wlb_listener *listener);
bool wlb_signal_emit(struct wlb_signal *signal, void *data);

bool wlb_listener_remove(struct wlb_listener *listener);

#endif // WLB_SIGNAL_H_
