#ifndef WLB_REGISTRY_H_
#define WLB_REGISTRY_H_

#include <wlblocks/signal.h>
#include <utl/common.h>
#include <utl/hash_map.h>
#include <utl/vector.h>
#include <stdint.h>
#include <wayland-client.h>

struct wlb_registry_global {
    struct wlb_registry_interface *interface;
    uint32_t name;
    void *global;
};

struct wlb_registry_interface {
    const struct wl_interface *wl_interface;
    uint32_t version;
    struct utl_vector globals; // uint32_t

    struct {
        struct wlb_signal add;    // const wlb_registry_global
        struct wlb_signal remove; // wlb_registry_global
    } signals;
};

struct wlb_registry {
    bool registered;
    struct utl_hash_map globals, interfaces;
};

bool wlb_registry_init(struct wlb_registry *registry);
bool wlb_registry_destroy(struct wlb_registry *registry);
bool wlb_registry_elements_destroy(struct wlb_registry *registry, utl_iterator_func iterator, void *data);

bool wlb_registry_register(struct wlb_registry *registry, struct wl_display *display, struct wl_registry *wl_registry);

struct wlb_registry_interface *wlb_registry_add_interface(struct wlb_registry *registry, const struct wl_interface *interface, uint32_t version, struct wlb_listener *add, struct wlb_listener *remove);

struct wlb_registry_interface *wlb_registry_get_interface(struct wlb_registry *registry, const struct wl_interface *interface);
struct wlb_registry_global *wlb_registry_get_global(struct wlb_registry *registry, uint32_t name);
struct wlb_registry_global *wlb_registry_get_single(struct wlb_registry *registry, const struct wl_interface *interface);

#endif // WLB_REGISTRY_H_
