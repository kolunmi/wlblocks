#ifndef WLB_GLDRAW_H_
#define WLB_GLDRAW_H_

#include <wlblocks/basic.h>
#include <wlblocks/theme.h>

/** Setup the 2D viewport.
 *
 * @param width the width of the viewport.
 * @param height the height of the viewport.
 *
 * @return this function always returns true.
 */
bool wlb_gl_make_viewport(int width, int height);

/** @struct wlb_texture
 *
 * @brief A gpu texture.
 *
 * Used for drawing images.
 */
struct wlb_texture {
  unsigned int id;
  int width;
  int height;
  int mipmaps;
  int format;
};

/** Load a texture into the gpu.
 *
 * @param dest the texture to load into.
 * @param data the image data.
 * @param width the width of the image.
 * @param height the height of the image.
 * @param mipmaps the image's mipmaps.
 * @param format the format of the image.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_texture_load(struct wlb_texture *dest,
                         struct wlb_data data,
                         int width, int height,
                         int mipmaps, int format);

/** Convert a cairo surface to a texture.
 *
 * @param dest the texture to load into.
 * @param surface the cairo surface.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_texture_from_cairo_surface(struct wlb_texture *dest,
                                       cairo_surface_t *surface);

/** Create a text texture.
 *
 * @param dest the texture to load into.
 * @param font the text font.
 * @param text the text data.
 * @param scale the text scale.
 * @param fg the text foreground.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_create_text_texture(struct wlb_texture *dest,
                                struct wlb_font *font,
                                const char *text,
                                double scale,
                                struct wlb_color *fg);

/** Create an svg texture.
 *
 * @param dest the texture to load into.
 * @param data the svg data.
 * @param width the texture width.
 * @param height the texture height.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_create_svg_texture(struct wlb_texture *dest,
			       struct wlb_data data,
			       int width, int height);

/** Draw a texture.
 *
 * @param texture the texture to draw.
 * @param source the source rect on texture.
 * @param dest the destination rect.
 * @param tint the color modulation.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_texture_draw(struct wlb_texture *texture,
                         struct wlb_rectf source,
                         struct wlb_rectf dest,
                         struct wlb_color *tint);

/** Draw a texture with simple arguments.
 *
 * @param texture the texture to draw.
 * @param position the destination position.
 * @param tint the color modulation.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_texture_draw_easy(struct wlb_texture *texture,
			      struct wlb_vec2 position,
			      struct wlb_color *tint);

/** Draw a texture with extra arguments.
 *
 * @param texture the texture to draw.
 * @param source the source rect on texture.
 * @param dest the destination rect.
 * @param tint the color modulation.
 * @param origin the origin.
 * @param rotation the rotation with respect to origin in radians.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_texture
 */
bool wlb_gl_texture_draw_extended(struct wlb_texture *texture,
                                  struct wlb_rectf source,
                                  struct wlb_rectf dest,
                                  struct wlb_color *tint,
                                  struct wlb_vec2 origin,
                                  float rotation);

/** @struct wlb_render_texture
 *
 * @brief A gpu render texture.
 *
 * Used for drawing on a texture.
 */
struct wlb_render_texture {
  unsigned int id;
  struct wlb_texture texture;
  struct wlb_texture depth;
};

/** Load a render texture into the gpu.
 *
 * @param dest the render texture to load into.
 * @param width the desired width.
 * @param width the desired height.
 *
 * @return true on success, false on failure.
 *
 * @memberof wlb_render_texture
 */
bool wlb_gl_render_texture_load(struct wlb_render_texture *dest, int width, int height);

/** Begin drawing to a render texture.
 *
 * @param rtexture the render texture.
 *
 * @return this function always returns true unless invalid args are passed.
 */
bool wlb_gl_begin_texture(struct wlb_render_texture *rtexture);

/** End drawing to a render texture.
 *
 * @param width the width of the returning viewport.
 * @param height the height of the returning viewport.
 *
 * @return this function always returns true.
 */
bool wlb_gl_end_texture(int width, int height);

/** Draw a rectangle.
 *
 * @param rect the rectangle.
 * @param color the color.
 *
 * @return true on success, false on failure.
 */
bool wlb_gl_draw_rect(struct wlb_rectf rect, struct wlb_color *color);

#endif // WLB_GLDRAW_H_
