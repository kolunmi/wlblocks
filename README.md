<div align="center">
<img src="data/logo.png" width=30% height=30%>
</div>

# wlblocks

## building
```sh
meson setup build
ninja -C build
```

## todo
+ [ ] Figure out the front end of things. Do we want to have things under related contexts? Or a simple wayland connection manager object? Then the user can 
    use this object to create things like a window or a popup or a layer surface? Should we abstract these ideas out? How would we represent them?
+ [X] Get generic buffers and formats working. For now, I think we should just use wl_shm formats. But we might end up using drm_fourcc codes depending on what happens.
+ [X] Allow the windows to take in buffers. Then release them back to the user.
+ [X] Get a backend for the wlb_window working. We might have to make some weird changes to get it working though. Esspecially if we want to have parent child relationships with xdg_surfaces.
      Considering that the wlr_layer_surface doesn't act in the same way.
+ [X] Remove the pointer owner stuff.
+ [ ] Get seat working.
+ [ ] Get xdg-shell working.
+ [ ] Maybe support cursor shape.
